pub(crate) const GITHUB_OAUTH_SECRET: &str = "github_oath_secret";
pub(crate) const SESSION: &str = "session";
pub(crate) const NEW_USER_AUTH: &str = "new_user_auth";
pub(crate) const AUTH_SRC: &str = "auth_src";
