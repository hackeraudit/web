use {RequestResult, Session};
use {common, models};
use ctx;
use data;
use data::SourceId;
use error::NotFound;
use error::ToError;
use failure::Error;
use hackeraudit_api_common::UsageStats;
use num::ToPrimitive;
use rocket::request::FlashMessage;
use rocket::request::Form;
use rocket::response::{Flash, Redirect};
use semver;
use tpl;

#[get("/package/<source>/<name>")]
fn package(
    ctx: ctx::Ctx,
    source: String,
    name: String,
    flash: Option<FlashMessage>,
) -> RequestResult<tpl::Rendered> {
    ctx.rate_limit()?;
    const PER_PAGE: usize = 200;
    let source_id = SourceId::from_str_default(&source);
    let versions = models::Package::query_versions(source_id, &name, 0, PER_PAGE as i64, &ctx.db)?;

    if versions.is_empty() {
        Err(Error::from(NotFound))?;
    }

    let last_version = versions[0];
    let mut stats = UsageStats::empty();
    let total_map = models::PackageVersion::calculate_stats_total(&last_version, &ctx.db)?;

    stats.total = common::map_to_usage_stat_item(total_map);
    let review_list = models::PackageVersion::load_reviews(&last_version, &ctx.db)?;

    let data = tpl::pkg::Data {
        base: ctx.base_data(flash),
        source_id: data::SourceId::from_str_default(&source),
        info_url: info_url(source_id, &name),
        pkg_name: name,
        last_pkg_version: last_version.to_simplesemver(),
        versions: versions.into_iter().map(|v| v.to_simplesemver()).collect(),
        reviews: review_list
            .into_iter()
            .map(|mr| tpl::review::Data {
                username: mr.username,
                user_trust_count: mr.user_trust_count as u32,
                pkg_version: mr.version,
                pkg_name: mr.name,
                comment: mr.comment,
                score: mr.score,
                source_id: mr.source_id,
            })
            .collect(),
        stats: tpl::stats::Data {
            stats,
            short_stats: true,
        },
    };

    Ok(tpl::render(&tpl::pkg_tpl(), &data))
}

#[derive(Serialize, FromForm)]
struct Review {
    score: String,
    comment: Option<String>,
}

impl Default for Review {
    fn default() -> Self {
        Review {
            score: "none".into(),
            comment: Some("".into()),
        }
    }
}

impl From<models::PackageVersionReview> for Review {
    fn from(pa: models::PackageVersionReview) -> Self {
        Review {
            score: models::ReviewScore::from_i16_default(pa.score)
                .to_str()
                .into(),
            comment: Some(pa.comment),
        }
    }
}

#[get("/package/<source>/<name>/<version>")]
fn package_version(
    ctx: ctx::Ctx,
    source: String,
    name: String,
    version: String,
    session: Option<Session>,
    flash: Option<FlashMessage>,
) -> RequestResult<tpl::Rendered> {
    ctx.rate_limit()?;
    let sv = semver::Version::parse(&version).into_error()?;

    let source_id = SourceId::from_str_default(&source);
    let pkg_ver = models::PackageVersion::load(source_id, &name, &sv, &ctx.db)?
        .ok_or(NotFound)
        .into_error()?;

    let review = if let Some(ref session) = session {
        Some(
            models::PackageVersionReview::load(session.uid, pkg_ver.id, &ctx.db)?
                .map(Review::from)
                .unwrap_or_default(),
        )
    } else {
        None
    };

    let stats = common::calculate_package_stats(&pkg_ver, &ctx.db)?;

    let review_list = models::PackageVersion::load_reviews(&pkg_ver, &ctx.db)?;

    let data = tpl::pkgver::Data {
        base: ctx.base_data(flash),
        source_id: data::SourceId::from_str_default(&source),
        info_url: info_url(source_id, &name),
        download_url: download_url(source_id, &name, &version),
        pkg_name: name,
        pkg_version: pkg_ver.to_simplesemver(),
        review: review.map(|r| tpl::pkgver::Review {
            score: data::ReviewScore::from_str_default(&r.score),
            comment: r.comment.unwrap_or("".into()),
        }),
        stats: tpl::stats::Data {
            stats,
            short_stats: false,
        },
        reviews: review_list
            .into_iter()
            .map(|mr| tpl::review::Data {
                username: mr.username,
                user_trust_count: mr.user_trust_count as u32,
                pkg_version: mr.version,
                pkg_name: mr.name,
                comment: mr.comment,
                score: mr.score,
                source_id: mr.source_id,
            })
            .collect(),
    };

    Ok(tpl::render(&tpl::pkgver_tpl(), &data))
}

#[post("/package/<source>/<name>/<version>/review", data = "<review>")]
fn package_version_review(
    ctx: ctx::Ctx,
    source: String,
    name: String,
    version: String,
    session: Session,
    review: Form<Review>,
) -> RequestResult<Flash<Redirect>> {
    ctx.rate_limit()?;

    let sv = semver::Version::parse(&version).into_error()?;
    let pkg_ver =
        models::PackageVersion::load(SourceId::from_str_default(&source), &name, &sv, &ctx.db)?
            .ok_or(NotFound)
            .into_error()?;

    if ctx.rate_limiter.review_pkg(session.uid, pkg_ver.id).is_ok() {
        let review = review.get();
        let _review = models::PackageVersionReview::create_or_update(
            session.uid,
            pkg_ver.id,
            models::ReviewScore::from_str_default(&review.score)
                .to_i16()
                .unwrap(),
            review.comment.as_ref().map(|s| s.as_str()).unwrap_or(""),
            &ctx.db,
        ).into_error()?;

        pkg_ver.recalculate_package_stats(&ctx.db)?;

        Ok(Flash::success(
            Redirect::to(&format!("/package/{}/{}/{}", source, name, version)),
            "Your review has been saved".to_owned(),
        ))
    } else {
        Ok(Flash::error(
            Redirect::to(&format!("/package/{}/{}/{}", source, name, version)),
            "Rate limit: try again in a minute or two".to_owned(),
        ))
    }
}

fn info_url(source_id: SourceId, name: &str) -> String {
    match source_id {
        SourceId::CratesIo => format!("https://crates.io/crates/{}", name),
    }
}

fn download_url(source_id: SourceId, name: &str, version: &str) -> String {
    match source_id {
        SourceId::CratesIo => format!(
            "https://crates.io/api/v1/crates/{}/{}/download",
            name, version
        ),
    }
}
