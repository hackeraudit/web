use {Result, Session};
use Outcome::*;
use db;
use rate_limit::HostOrUser;
use rate_limit::RateLimiter;
use rocket::State;
use rocket::http::Status;
use rocket::http::uri::URI;
use rocket::request::{self, FromRequest, Request};
use rocket::request::FlashMessage;
use std::fmt;
use std::net::SocketAddr;

use std::sync::Mutex;

#[derive(Debug, Fail)]
#[fail(display = "Unauthorized")]
pub struct Unauthorized {
    pub ctx: Mutex<Info>,
}

use tpl;

#[derive(Clone)]
pub struct Info {
    pub uri: String,
    pub remote: SocketAddr,
    pub session: Option<Session>,
}

impl fmt::Debug for Info {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Ctx {{ uri: {}, remote: {} }}", self.uri, self.remote)
    }
}

impl Info {
    pub fn base_data(&self, flash: Option<tpl::base::Flash>) -> tpl::base::Data {
        tpl::base::Data {
            title: "Hacker Audit".into(),
            search_query: None,
            current_url: self.uri.clone(),
            session: self.session.clone(),
            flash: flash,
        }
    }
}

pub struct Ctx {
    pub db: db::DbConn,
    pub rate_limiter: RateLimiter,
    pub info: Info,
}

impl<'a, 'r> FromRequest<'a, 'r> for Ctx {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<Ctx, ()> {
        let pool = request.guard::<State<db::Pool>>()?;
        let db = match pool.get() {
            Err(_) => return Failure((Status::ServiceUnavailable, ())),
            Ok(conn) => db::DbConn(conn),
        };

        let uri: &URI = match FromRequest::from_request(request) {
            Success(uri) => uri,
            _ => return Failure((Status::InternalServerError, ())),
        };

        let remote = match FromRequest::from_request(request) {
            Success(remote) => remote,
            _ => return Failure((Status::InternalServerError, ())),
        };

        let session = match FromRequest::from_request(request) {
            Success(session) => session,
            _ => return Failure((Status::InternalServerError, ())),
        };

        let rl = &*request.guard::<State<RateLimiter>>()?;

        Success(Ctx {
            db: db,
            rate_limiter: rl.clone(),
            info: Info {
                uri: uri.as_str().into(),
                remote: remote,
                session: session,
            },
        })
    }
}

impl Ctx {
    pub fn rate_limit(&self) -> Result<()> {
        self.rate_limiter
            .request(HostOrUser::H(self.info.remote.ip()))
    }

    pub fn get_session(&self) -> Result<&Session> {
        match self.info.session {
            None => Err(Unauthorized {
                ctx: Mutex::new(self.info.clone()),
            })?,
            Some(ref s) => Ok(s),
        }
    }

    pub fn base_data(&self, flash: Option<FlashMessage>) -> tpl::base::Data {
        self.info.base_data(flash.map(|f| tpl::base::Flash {
            is_error: f.name() == "error",
            msg: f.msg().to_string(),
        }))
    }
}
