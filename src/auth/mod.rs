use Result;
use cookie;
use ctx;
use db::DbConn;
use error::NotFound;
use failure;
use failure::{Fail, ResultExt};
use hubcaps::{self, Credentials, Github};
use models;
use rand::{self, Rng};
use reqwest;
use reqwest::header::qitem;
use reqwest::mime;
use rocket::http::{Cookie, Cookies};
use rocket::request::FlashMessage;
use rocket::request::Form;
use rocket::response::{Flash, Redirect};
use serde_json;
use std;
use std::env;
use std::time;
use tokio_core;
use tpl;

#[derive(FromForm)]
struct GithubCallback {
    code: String,
    state: String,
}

// TODO: Make this encrypted
// https://github.com/SergioBenitez/Rocket/issues/477
#[derive(FromForm, Debug)]
struct Src {
    src: String,
}

#[get("/auth/logout")]
fn logout(mut cookies: Cookies) -> Flash<Redirect> {
    cookies.remove_private(Cookie::named(cookie::SESSION));
    Flash::success(Redirect::to("/"), "You have been signed-out!".to_owned())
}

#[get("/oauth/github/login")]
fn github_login(cookies: Cookies) -> Redirect {
    github_login_src(None, cookies)
}

#[get("/oauth/github/login?<src>")]
fn github_login_src(src: Option<Src>, mut cookies: Cookies) -> Redirect {
    let client_id = env::var("GITHUB_OAUTH_CLIENT_ID").expect("GITHUB_OAUTH_CLIENT_ID must be set");
    cookies.add_private(Cookie::new(
        cookie::AUTH_SRC,
        src.map(|s| s.src).unwrap_or_else(|| "/".into()),
    ));

    let our_base_url = env::var("BASE_URL").expect("BASE_URL must be set");
    let state_str = rand::thread_rng()
        .gen_ascii_chars()
        .take(10)
        .collect::<String>();

    cookies.add_private(Cookie::new(cookie::GITHUB_OAUTH_SECRET, state_str.clone()));

    let callback_uri = our_base_url + "/oauth/github/callback";
    let redirect_uri = "https://github.com/login/oauth/authorize?client_id=".to_string()
        + &client_id + "&redirect_uri=" + &callback_uri + "&state="
        + &state_str + "&scope=user:email";

    Redirect::to(&redirect_uri)
}

fn github_get_user_info(
    _c: &reqwest::Client,
    token: &str,
) -> Result<hubcaps::users::AuthenticatedUser> {
    let mut core = tokio_core::reactor::Core::new().expect("reactor fail");
    let github = Github::new(
        "Hacker Audit",
        Some(Credentials::Token(token.into())),
        &core.handle(),
    );

    Ok(core.run(github.users().authenticated())
        .map_err(failure::SyncFailure::new)?)
}

fn github_get_access_token(c: &reqwest::Client, code: String) -> Result<String> {
    let client_id = env::var("GITHUB_OAUTH_CLIENT_ID").expect("GITHUB_OAUTH_CLIENT_ID must be set");
    let client_secret =
        env::var("GITHUB_OAUTH_CLIENT_SECRET").expect("GITHUB_OAUTH_CLIENT_SECRET must be set");

    let params = [
        ("client_id", client_id),
        ("client_secret", client_secret),
        ("code", code),
    ];
    let mut resp = c.post("https://github.com/login/oauth/access_token")
        .header(reqwest::header::Accept(vec![qitem(mime::APPLICATION_JSON)]))
        .form(&params)
        .send()?
        .error_for_status()?;

    if !resp.status().is_success() {
        return Err(failure::err_msg("Couldn't request the token"));
    }

    #[derive(Deserialize)]
    struct Response {
        access_token: String,
    }

    let json: Response = resp.json().context("Couldn't deserialize the response")?;

    Ok(json.access_token)
}

enum CallbackOutcome {
    New,
    Existing,
}

#[derive(Serialize, Deserialize)]
pub struct NewUserAuth {
    pub service: models::ServiceId,
    pub service_uid: String,
    // suggested
    pub username: String,
}

impl NewUserAuth {
    fn to_json(&self) -> serde_json::Result<String> {
        serde_json::to_string(&self)
    }

    fn delete_from(&self, cookies: &mut Cookies) {
        cookies.remove_private(Cookie::named(cookie::NEW_USER_AUTH));
    }

    fn from(cookies: &mut Cookies) -> Result<Self> {
        Ok(cookies
            .get_private(cookie::NEW_USER_AUTH)
            .ok_or_else(|| failure::Error::from(NotFound))
            .and_then(|cookie| serde_json::from_str(cookie.value()).map_err(failure::Error::from))?)
    }
}

fn github_callback_process(
    data: GithubCallback,
    db: &mut DbConn,
    cookies: &mut Cookies,
) -> Result<CallbackOutcome> {
    if let Some(cookie) = cookies.get_private(cookie::GITHUB_OAUTH_SECRET) {
        if cookie.value() != data.state {
            return Err(failure::err_msg("Wrong ouath state value"));
        }
    } else {
        return Err(failure::err_msg("Missing ouath state cookie"));
    }

    let c = reqwest::Client::builder()
        .timeout(time::Duration::from_secs(3))
        .build()
        .map_err(|e| e.context("Can't build http client for oauth"))?;

    let token = github_get_access_token(&c, data.code)?;
    let github_info = github_get_user_info(&c, &token)?;

    if let Some(user) =
        models::User::load_by_service_uid(models::ServiceId::Github, github_info.id, db)
            .context("Couldn't query user")?
    {
        user.sign_in(cookies)
            .map_err(|e| e.context("Couldn't serialize session!"))?;
        Ok(CallbackOutcome::Existing)
    } else {
        let a = NewUserAuth {
            service: models::ServiceId::Github,
            service_uid: format!("{}", github_info.id),
            username: github_info.login,
        }.to_json()
            .map_err(|e| e.context("Couldn't serialize new auth"))?;

        cookies.add_private(Cookie::new(cookie::NEW_USER_AUTH, a));
        Ok(CallbackOutcome::New)
    }
}

#[get("/oauth/github/callback?<data>")]
fn github_callback(
    data: Option<GithubCallback>,
    mut db: DbConn,
    mut cookies: Cookies,
) -> Flash<Redirect> {
    let src = cookies
        .get_private(cookie::AUTH_SRC)
        .map(|c| c.value().to_string())
        .unwrap_or_else(|| "/".into());

    if let Some(data) = data {
        match github_callback_process(data, &mut db, &mut cookies) {
            Ok(CallbackOutcome::Existing) => {
                Flash::success(Redirect::to(&src), "You are now signed in!".to_owned())
            }
            Ok(CallbackOutcome::New) => {
                Flash::success(Redirect::to("/register"), "Create user!".to_owned())
            }
            Err(e) => Flash::error(Redirect::to(&src), format!("Github oauth failed: {}!", e)),
        }
    } else {
        Flash::error(
            Redirect::to(&src),
            "Github oauth failed: couldn't parse callback data!",
        )
    }
}

#[derive(Debug, Fail)]
pub enum InvalidUsername {
    #[fail(display = "Username is too long.")] TooLong,
    #[fail(display = "Username is too short.")] TooShort,
    #[fail(display = "Username must contain ASCII numbers and lowercase letters only.")]
    InvalidCharacters,
}

#[derive(Debug, Fail)]
#[fail(display = "Must accept the rules")]
pub struct MustAcceptRules;

fn check_valid_username(s: &str) -> Result<()> {
    if s.len() < 3 {
        Err(InvalidUsername::TooShort)?;
    }
    if s.len() > 20 {
        Err(InvalidUsername::TooLong)?;
    }

    if !s.chars()
        .all(|c| c.is_ascii_digit() || (c.is_ascii_alphanumeric() && c.is_lowercase()))
    {
        Err(InvalidUsername::InvalidCharacters)?;
    }

    Ok(())
}

#[test]
fn valid_usernames() {
    assert!(check_valid_username("foo1234").is_ok());
    assert!(check_valid_username("1foo1234").is_ok());
    assert!(check_valid_username("łążęcz").is_err());
    assert!(check_valid_username("ab").is_err());
    assert!(check_valid_username("abdddddddddddddddddddddddddddd").is_err());
}

#[derive(Serialize, FromForm)]
struct CreateUser {
    username: String,
    agree: bool,
}

fn try_register(
    ctx: &ctx::Ctx,
    new_user_info: &NewUserAuth,
    cookies: &mut Cookies,
    user_data: &CreateUser,
) -> Result<()> {
    ctx.rate_limit()?;
    check_valid_username(&user_data.username)?;
    if !user_data.agree {
        Err(MustAcceptRules)?;
    }

    let user = models::User::create_with_auth(
        &user_data.username,
        new_user_info.service,
        &new_user_info.service_uid,
        &ctx.db,
    )?;
    user.sign_in(cookies).context("Can't sign in!")?;

    Ok(())
}

#[post("/register", data = "<user_data>")]
fn register_post(
    ctx: ctx::Ctx,
    mut cookies: Cookies,
    user_data: Form<CreateUser>,
) -> std::result::Result<Flash<Redirect>, tpl::Rendered> {
    let src = cookies
        .get_private(cookie::AUTH_SRC)
        .map(|c| c.value().to_string())
        .unwrap_or_else(|| "/".into());

    match NewUserAuth::from(&mut cookies) {
        Ok(new_user_info) => {
            match try_register(&ctx, &new_user_info, &mut cookies, user_data.get()) {
                Ok(()) => {
                    new_user_info.delete_from(&mut cookies);
                    Ok(Flash::success(
                        Redirect::to(&src),
                        "User account created!".to_owned(),
                    ))
                }
                Err(e) => {
                    let msg = if let Ok(Some(_)) =
                        models::User::load_by_username(&user_data.get().username, &ctx.db)
                    {
                        "Username already registered!".into()
                    } else {
                        format!("Couldn't create user: {}", e)
                    };

                    let data = tpl::register::Data {
                        username: user_data.get().username.clone(),
                        base: ctx.info.base_data(Some(tpl::base::Flash {
                            is_error: true,
                            msg: msg,
                        })),
                    };
                    Err(tpl::render(&tpl::register_tpl(), &data))
                }
            }
        }
        Err(e) => Ok(Flash::error(Redirect::to(&src), e.to_string())),
    }
}

#[get("/register")]
fn register_get(
    ctx: ctx::Ctx,
    mut cookies: Cookies,
    flash: Option<FlashMessage>,
) -> std::result::Result<Flash<Redirect>, tpl::Rendered> {
    match NewUserAuth::from(&mut cookies) {
        Ok(auth) => {
            let data = tpl::register::Data {
                username: auth.username,
                base: ctx.base_data(flash),
            };
            Err(tpl::render(&tpl::register_tpl(), &data))
        }
        Err(e) => Ok(Flash::error(Redirect::to("/"), e.to_string())),
    }
}
