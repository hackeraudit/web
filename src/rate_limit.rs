use Result;
use lru_time_cache::LruCache;
use models::{PkgVerId, UserId};
use std;
use std::sync::{Arc, Mutex};
use std::time::Duration;

struct RateLimitInner {
    request_30s: LruCache<HostOrUser, i8>,
    audit_5min: LruCache<HostOrUser, i8>,
    review_5min: LruCache<UserId, i8>,
    review_pkg_5min: LruCache<(UserId, PkgVerId), i8>,
}

#[derive(Clone)]
pub struct RateLimiter {
    inner: Arc<Mutex<RateLimitInner>>,
}

#[derive(Ord, PartialOrd, PartialEq, Eq, Clone, Debug)]
pub enum HostOrUser {
    H(std::net::IpAddr),
    U(UserId),
}

#[derive(Debug, Fail)]
#[fail(display = "Request was rate limited")]
pub struct RateLimitErr;

impl RateLimiter {
    pub fn new() -> Self {
        RateLimiter {
            inner: Arc::new(Mutex::new(RateLimitInner {
                request_30s: LruCache::with_expiry_duration(Duration::from_secs(10)),
                audit_5min: LruCache::with_expiry_duration(Duration::from_secs(5 * 60)),
                review_5min: LruCache::with_expiry_duration(Duration::from_secs(5 * 60)),
                review_pkg_5min: LruCache::with_expiry_duration(Duration::from_secs(5 * 60)),
            })),
        }
    }

    pub fn request(&self, key: HostOrUser) -> Result<()> {
        let mut inner = self.inner.lock().unwrap();
        let inner = &mut inner.request_30s;
        let cur = *inner.peek(&key).unwrap_or(&0);
        if cur >= 20 {
            Err(RateLimitErr)?;
        }

        inner.insert(key, cur + 1);

        Ok(())
    }

    pub fn audit(&self, key: HostOrUser) -> Result<()> {
        let mut inner = self.inner.lock().unwrap();
        let inner = &mut inner.audit_5min;
        let cur = *inner.peek(&key).unwrap_or(&0);
        if cur >= 2 {
            Err(RateLimitErr)?;
        }

        inner.insert(key, cur + 1);

        Ok(())
    }

    pub fn review_pkg(&self, uid: UserId, pkgverid: PkgVerId) -> Result<()> {
        let mut inner = self.inner.lock().unwrap();
        let cur = *inner.review_pkg_5min.peek(&(uid, pkgverid)).unwrap_or(&0);

        if cur == 0 {
            // touching a new package
            let cur = *inner.review_5min.peek(&uid).unwrap_or(&0);

            if cur >= 3 {
                Err(RateLimitErr)?;
            }
            inner.review_5min.insert(uid, cur + 1);
        }

        if cur >= 10 {
            Err(RateLimitErr)?;
        }

        inner.review_pkg_5min.insert((uid, pkgverid), cur + 1);

        Ok(())
    }
}
