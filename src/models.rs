use diesel;
use diesel::QueryResult;
use diesel::pg::Pg;
use diesel::pg::PgConnection;
use diesel::prelude::*;

use super::schema::{auths, package_version_reviews, package_versions, packages,
                    pkgver_major_counts, pkgver_minor_counts, pkgver_patch_counts,
                    pkgver_total_counts, user_flags, user_trusts, users};
use cookie;
use data::SourceId;
use diesel::query_source::QueryableByName;
use diesel::row::NamedRow;
use rocket::http::{Cookie, Cookies};
use semver;
use std::result;

pub use data::{ReviewScore, SimpleSemver};
use hackeraudit_api_common as api_common;

use Result;
use Session;
use chrono;
use data;
use error::NotFound;
use num::ToPrimitive;
use std;
use std::collections::BTreeMap;
use std::convert::TryInto;

use diesel::types::{BigInt, Bool, Integer, SmallInt};

use insideout::InsideOut;

pub type UserId = i32;
pub type PkgId = i32;
pub type PkgVerId = i64;
use data::Count;
//enable_multi_table_joins!(packages, package_version_reviews);
//enable_multi_table_joins!(package_versions, pkgver_patch_counts);

// {{{ users
#[derive(Queryable, Serialize, Identifiable)]
#[table_name = "users"]
#[allow(unused)]
#[has_many(auths)]
pub struct User {
    pub id: UserId,
    pub username: String,
    // real name
    pub name: String,
    // bio
    pub about: String,
    pub token: String,
    pub trust_dst_count: i32,
    pub flag_dst_count: i32,
}

#[derive(Insertable)]
#[table_name = "users"]
pub struct NewUser<'a> {
    pub username: &'a str,
}

impl User {
    pub(crate) fn load_by_service_uid(
        service: ServiceId,
        id: u64,
        conn: &PgConnection,
    ) -> QueryResult<Option<User>> {
        if let Some(auth) = Auth::load_by_service_uid(service, id, conn)? {
            Ok(User::load_by_auth(&auth, conn)?)
        } else {
            Ok(None)
        }
    }

    pub(crate) fn load_by_username(uname: &str, conn: &PgConnection) -> Result<Option<User>> {
        let res = users::table
            .filter(users::username.eq(uname.to_string()))
            .first::<User>(conn)
            .optional()?;

        Ok(res)
    }

    pub(crate) fn load_by_token(token: &str, conn: &PgConnection) -> Result<Option<User>> {
        let res = users::table
            .filter(users::token.eq(token.to_string()))
            .first::<User>(conn)
            .optional()?;

        Ok(res)
    }
    pub(crate) fn load_by_auth(auth: &Auth, conn: &PgConnection) -> QueryResult<Option<User>> {
        users::table.find(auth.user_id).first(conn).optional()
    }

    pub(crate) fn create(username: &str, conn: &PgConnection) -> QueryResult<User> {
        let new_u = NewUser { username: username };

        diesel::insert_into(users::table)
            .values(&new_u)
            .get_result(conn)
    }

    pub(crate) fn load_most_trusted(conn: &PgConnection) -> Result<Vec<User>> {
        let res = users::table
            .limit(20)
            .order(users::trust_dst_count.desc())
            .load(conn)?;

        Ok(res)
    }
    pub(crate) fn create_with_auth(
        username: &str,
        service: ServiceId,
        service_uid: &str,
        conn: &PgConnection,
    ) -> QueryResult<User> {
        conn.transaction(|| -> diesel::QueryResult<User> {
            let user = User::create(username, conn)?;
            let _auth = Auth::create(&user, service, service_uid, conn)?;
            Ok(user)
        })
    }

    pub(crate) fn sign_in(&self, cookies: &mut Cookies) -> Result<()> {
        let s = Session::from_user(&self).to_json()?;
        cookies.add_private(Cookie::new(cookie::SESSION, s));
        Ok(())
    }
}
// }}}

// {{{ auths
#[derive(Queryable, Identifiable)]
#[table_name = "auths"]
#[allow(unused)]
#[belongs_to(user)]
pub struct Auth {
    pub id: i32,
    pub user_id: UserId,
    pub service: i16,
    pub service_uid: String,
}

#[derive(FromPrimitive, ToPrimitive, Serialize, Deserialize, Copy, Clone)]
pub enum ServiceId {
    Github = 0,
}

#[derive(Serialize, Deserialize, Insertable)]
#[table_name = "auths"]
pub struct NewAuth<'a> {
    pub user_id: UserId,
    pub service: i16,
    pub service_uid: &'a str,
}

impl Auth {
    pub(crate) fn load_by_service_uid(
        ser_id: ServiceId,
        uid: u64,
        conn: &PgConnection,
    ) -> QueryResult<Option<Auth>> {
        let mut res = auths::table
            .filter(auths::service.eq(ser_id.to_i16().unwrap()))
            .filter(auths::service_uid.eq(format!("{}", uid)))
            .load::<Auth>(conn)?;

        Ok(res.pop())
    }

    pub(crate) fn create(
        user: &User,
        service: ServiceId,
        service_uid: &str,
        conn: &PgConnection,
    ) -> QueryResult<Auth> {
        let new_a = NewAuth {
            user_id: user.id,
            service: service.to_i16().unwrap(),
            service_uid,
        };

        diesel::insert_into(auths::table)
            .values(&new_a)
            .get_result(conn)
    }
}
// }}}

// {{{ packages
#[derive(Queryable, Identifiable)]
#[table_name = "packages"]
#[allow(unused)]
#[has_many(package_versions)]
pub struct Package {
    pub id: i32,
    pub source: i16,
    pub name: String,
}

#[derive(Insertable)]
#[table_name = "packages"]
pub struct NewPackage<'a> {
    pub source: i16,
    pub name: &'a str,
}

impl Package {
    #[allow(unused)]
    pub(crate) fn load_by_name(
        s: SourceId,
        n: &str,
        conn: &PgConnection,
    ) -> QueryResult<Option<Package>> {
        use num::ToPrimitive;
        let mut res = packages::table
            .filter(packages::source.eq(s.to_i16().unwrap()))
            .filter(packages::name.eq(n.to_string()))
            .load::<Package>(conn)?;

        Ok(res.pop())
    }

    fn load_or_create(s: SourceId, name: &str, conn: &PgConnection) -> Result<Package> {
        let new = NewPackage {
            source: s.to_i16().unwrap(),
            name: name,
        };

        conn.transaction(|| {
            Ok(diesel::insert_into(packages::table)
                .values(&new)
                .on_conflict_do_nothing()
                .get_result(conn)
                .optional()
                .inside_out()
                .or_else(|| Package::load_by_name(s, name, conn).inside_out())
                .ok_or(NotFound)??)
        })
    }

    pub(crate) fn query_names(
        q: &str,
        offset: i64,
        limit: i64,
        conn: &PgConnection,
    ) -> QueryResult<Vec<(i16, String)>> {
        packages::table
            .select((packages::source, packages::name))
            .filter(packages::name.like(format!("%{}%", q.to_string())))
            .offset(offset)
            .limit(limit)
            .load(conn)
    }

    pub(crate) fn query_versions(
        source_id: SourceId,
        name: &str,
        offset: i64,
        limit: i64,
        conn: &PgConnection,
    ) -> Result<Vec<PackageVersion>> {
        let mut rows: Vec<(Package, PackageVersion)> = packages::table
            .inner_join(package_versions::table)
            .filter(packages::source.eq(source_id.to_i16().unwrap()))
            .filter(packages::name.eq(name))
            .order((
                package_versions::major.desc(),
                package_versions::minor.desc(),
                package_versions::patch.desc(),
                package_versions::pre.desc(),
            ))
            .offset(offset)
            .limit(limit)
            .load(conn)?;

        let r = rows.drain(..).map(|(_c, v)| v).collect();
        Ok(r)
    }
}
// }}}

// {{{ package versions
#[derive(Queryable, Identifiable, Associations, Clone, Copy)]
#[table_name = "package_versions"]
#[belongs_to(Package)]
pub struct PackageVersion {
    pub id: PkgVerId,
    pub package_id: PkgId,
    pub major: i16,
    pub minor: i16,
    pub patch: i16,
    pub pre: bool,
    pub created_at: chrono::NaiveDateTime,
}

#[derive(Insertable)]
#[table_name = "package_versions"]
pub struct NewPackageVersion {
    pub package_id: i32,
    pub major: i16,
    pub minor: i16,
    pub patch: i16,
    pub pre: bool,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Queryable)]
struct ScoreAndCount(#[column_name(score)] i16, #[column_name(count)] i64);

impl QueryableByName<Pg> for ScoreAndCount {
    fn build<R: NamedRow<Pg>>(
        row: &R,
    ) -> result::Result<Self, Box<std::error::Error + Send + Sync>> {
        Ok(ScoreAndCount(
            row.get::<SmallInt, _>("score").unwrap_or(0),
            row.get::<BigInt, _>("count")?,
        ))
    }
}

impl PackageVersion {
    #[allow(unused)]
    pub(crate) fn into_semver(self) -> semver::Version {
        self.to_semver()
    }

    pub(crate) fn to_semver(&self) -> semver::Version {
        SimpleSemver {
            major: self.major,
            minor: self.minor,
            patch: self.patch,
            pre: self.pre,
        }.into()
    }

    pub(crate) fn to_simplesemver(&self) -> SimpleSemver {
        SimpleSemver {
            major: self.major,
            minor: self.minor,
            patch: self.patch,
            pre: self.pre,
        }
    }

    pub(crate) fn load_usage_stats(&self, db: &PgConnection) -> Result<api_common::UsageStats> {
        let total = pkgver_total_counts::table
            .filter(pkgver_total_counts::package_id.eq(self.package_id))
            .first::<PkgVerTotalCounts>(&*db)
            .optional()?;

        let major = pkgver_major_counts::table
            .filter(pkgver_major_counts::package_id.eq(self.package_id))
            .filter(pkgver_major_counts::major.eq(self.major))
            .first::<PkgVerMajorCounts>(&*db)
            .optional()?;

        let minor = pkgver_minor_counts::table
            .filter(pkgver_minor_counts::package_id.eq(self.package_id))
            .filter(pkgver_minor_counts::major.eq(self.major))
            .filter(pkgver_minor_counts::minor.eq(self.minor))
            .first::<PkgVerMinorCounts>(&*db)
            .optional()?;

        let patch = pkgver_patch_counts::table
            .filter(pkgver_patch_counts::package_id.eq(self.package_id))
            .filter(pkgver_patch_counts::major.eq(self.major))
            .filter(pkgver_patch_counts::minor.eq(self.minor))
            .filter(pkgver_patch_counts::patch.eq(self.patch))
            .filter(pkgver_patch_counts::pre.eq(self.pre))
            .first::<PkgVerPatchCounts>(&*db)
            .optional()?;

        let empty = api_common::UsageStatsItem {
            good: 0,
            bad: 0,
            used: 0,
        };

        Ok(api_common::UsageStats {
            total: total
                .map(|s| api_common::UsageStatsItem {
                    good: s.good,
                    bad: s.bad,
                    used: s.used,
                })
                .unwrap_or_else(|| empty),
            major: major
                .map(|s| api_common::UsageStatsItem {
                    good: s.good,
                    bad: s.bad,
                    used: s.used,
                })
                .unwrap_or_else(|| empty),
            minor: minor
                .map(|s| api_common::UsageStatsItem {
                    good: s.good,
                    bad: s.bad,
                    used: s.used,
                })
                .unwrap_or_else(|| empty),
            patch: patch
                .map(|s| api_common::UsageStatsItem {
                    good: s.good,
                    bad: s.bad,
                    used: s.used,
                })
                .unwrap_or_else(|| empty),
        })
    }
    pub(crate) fn recalculate_package_stats(&self, db: &PgConnection) -> Result<()> {
        let patch = PackageVersion::calculate_stats_patch(self, db)?;
        let minor = PackageVersion::calculate_stats_minor(self, db)?;
        let major = PackageVersion::calculate_stats_major(self, db)?;
        let total = PackageVersion::calculate_stats_total(self, db)?;

        PkgVerTotalCounts::create_or_update(
            self.package_id,
            data::Counts {
                used: *total.get(&ReviewScore::Used).unwrap_or(&0),
                bad: *total.get(&ReviewScore::Bad).unwrap_or(&0),
                good: *total.get(&ReviewScore::Good).unwrap_or(&0),
            },
            db,
        )?;

        PkgVerMajorCounts::create_or_update(
            self.package_id,
            self.major,
            data::Counts {
                used: *major.get(&ReviewScore::Used).unwrap_or(&0),
                bad: *major.get(&ReviewScore::Bad).unwrap_or(&0),
                good: *major.get(&ReviewScore::Good).unwrap_or(&0),
            },
            db,
        )?;

        PkgVerMinorCounts::create_or_update(
            self.package_id,
            self.major,
            self.minor,
            data::Counts {
                used: *minor.get(&ReviewScore::Used).unwrap_or(&0),
                bad: *minor.get(&ReviewScore::Bad).unwrap_or(&0),
                good: *minor.get(&ReviewScore::Good).unwrap_or(&0),
            },
            db,
        )?;

        PkgVerPatchCounts::create_or_update(
            self.package_id,
            self.major,
            self.minor,
            self.patch,
            self.pre,
            data::Counts {
                used: *patch.get(&ReviewScore::Used).unwrap_or(&0),
                bad: *patch.get(&ReviewScore::Bad).unwrap_or(&0),
                good: *patch.get(&ReviewScore::Good).unwrap_or(&0),
            },
            db,
        )?;

        Ok(())
    }

    pub(crate) fn create_if_not_exists(
        s: SourceId,
        name: &str,
        version: &semver::Version,
        conn: &PgConnection,
    ) -> Result<PackageVersion> {
        let package = Package::load_or_create(s, name, conn)?;

        let SimpleSemver {
            major,
            minor,
            patch,
            pre,
        } = version.try_into()?;

        let new = NewPackageVersion {
            package_id: package.id,
            major: major,
            minor: minor,
            patch: patch,
            pre: pre,
        };

        Ok(diesel::insert_into(package_versions::table)
            .values(&new)
            .on_conflict_do_nothing()
            .get_result(conn)?)
    }

    pub(crate) fn load(
        source_id: SourceId,
        name: &str,
        version: &semver::Version,
        conn: &PgConnection,
    ) -> Result<Option<PackageVersion>> {
        let SimpleSemver {
            major,
            minor,
            patch,
            pre,
        } = version.try_into()?;
        let res: Option<(Package, PackageVersion)> = packages::table
            .inner_join(package_versions::table)
            .filter(packages::source.eq(source_id.to_i16().unwrap()))
            .filter(packages::name.eq(name))
            .filter(package_versions::major.eq(major))
            .filter(package_versions::minor.eq(minor))
            .filter(package_versions::patch.eq(patch))
            .filter(package_versions::pre.eq(pre))
            .first(conn)
            .optional()?;
        Ok(res.map(|x| x.1))
    }

    pub(crate) fn load_most_wanted(
        source_id: SourceId,
        conn: &PgConnection,
    ) -> Result<Vec<(Package, PackageVersion, PkgVerPatchCounts)>> {
        let res: Vec<(Package, PackageVersion, PkgVerPatchCounts)> = packages::table
            .inner_join(package_versions::table)
            .inner_join(
                pkgver_patch_counts::table.on(packages::id
                    .eq(pkgver_patch_counts::package_id)
                    .and(package_versions::major.eq(pkgver_patch_counts::major))
                    .and(package_versions::minor.eq(pkgver_patch_counts::minor))
                    .and(package_versions::patch.eq(pkgver_patch_counts::patch))
                    .and(package_versions::pre.eq(pkgver_patch_counts::pre))),
            )
            .filter(packages::source.eq(source_id.to_i16().unwrap()))
            .limit(20)
            .order(pkgver_patch_counts::review_ratio_cents.desc())
            .load(conn)?;

        Ok(res)
    }

    pub(crate) fn load_reviews(&self, conn: &PgConnection) -> Result<Vec<ReviewData>> {
        let list: Vec<
            (
                i16,
                String,
                i16,
                String,
                UserId,
                i16,
                i16,
                i16,
                bool,
                UserId,
                String,
                i32,
            ),
        > = package_version_reviews::table
            .inner_join(users::table)
            .inner_join(package_versions::table.inner_join(packages::table))
            .select((
                packages::source,
                packages::name,
                package_version_reviews::score,
                package_version_reviews::comment,
                package_version_reviews::user_id,
                package_versions::major,
                package_versions::minor,
                package_versions::patch,
                package_versions::pre,
                users::id,
                users::username,
                users::trust_dst_count,
            ))
            .filter(package_version_reviews::package_version_id.eq(self.id))
            .load(conn)?;

        let list = list.into_iter()
            .map(|s| ReviewData {
                source_id: SourceId::from_i16_default(s.0),
                name: s.1,
                version: SimpleSemver {
                    major: s.5,
                    minor: s.6,
                    patch: s.7,
                    pre: s.8,
                }.into(),
                score: ReviewScore::from_i16_default(s.2),
                comment: s.3,
                user_id: s.9,
                username: s.10,
                user_trust_count: s.11,
            })
            .collect();

        Ok(list)
    }

    pub(crate) fn calculate_stats_patch(
        &self,
        conn: &PgConnection,
    ) -> Result<BTreeMap<ReviewScore, i64>> {
        let res = {
            let query = diesel::sql_query(
                "SELECT score, count(DISTINCT package_version_reviews.user_id) as count \
                 FROM package_versions \
                 INNER JOIN package_version_reviews \
                 ON package_versions.id =
                 package_version_reviews.package_version_id \
                 WHERE
                 package_versions.package_id = $1 \
                 AND package_versions.major = $2 \
                 AND package_versions.minor = $3 \
                 AND package_versions.patch = $4 \
                 AND package_versions.pre = $5 \
                 GROUP BY score",
            );
            let query = query
                .bind::<Integer, _>(self.package_id)
                .bind::<SmallInt, _>(self.major)
                .bind::<SmallInt, _>(self.minor)
                .bind::<SmallInt, _>(self.patch)
                .bind::<Bool, _>(self.pre);

            query.load::<ScoreAndCount>(conn)?
        };

        let res_used = {
            let query = diesel::sql_query(
                "SELECT count(DISTINCT package_version_reviews.user_id) as count \
                 FROM package_versions \
                 INNER JOIN package_version_reviews \
                 ON package_versions.id =
                 package_version_reviews.package_version_id \
                 WHERE
                 package_versions.package_id = $1 \
                 AND package_versions.major = $2 \
                 AND package_versions.minor = $3 \
                 AND package_versions.patch = $4 \
                 AND package_versions.pre = $5 \
                 AND package_version_reviews.score <> $6 \
                 ",
            );

            let query = query
                .bind::<Integer, _>(self.package_id)
                .bind::<SmallInt, _>(self.major)
                .bind::<SmallInt, _>(self.minor)
                .bind::<SmallInt, _>(self.patch)
                .bind::<Bool, _>(self.pre)
                .bind::<SmallInt, _>(ReviewScore::None.to_i16().unwrap());

            query.load::<ScoreAndCount>(conn)?
        };

        let mut res: BTreeMap<_, _> = res.iter()
            .cloned()
            .map(|sc| (ReviewScore::from_i16_default(sc.0), sc.1))
            .collect();

        res.insert(ReviewScore::Used, res_used[0].1);
        Ok(res)
    }

    pub(crate) fn calculate_stats_minor(
        &self,
        conn: &PgConnection,
    ) -> Result<BTreeMap<ReviewScore, i64>> {
        let res = {
            let query = diesel::sql_query(
                "SELECT score, count(DISTINCT package_version_reviews.user_id) AS count \
                 FROM package_versions \
                 INNER JOIN package_version_reviews \
                 ON package_versions.id =
                 package_version_reviews.package_version_id \
                 WHERE \
                 package_versions.package_id = $1 \
                 AND package_versions.major = $2 \
                 AND package_versions.minor = $3 \
                 AND package_versions.patch <= $4 \
                 AND package_versions.pre = FALSE \
                 GROUP BY score",
            );
            let query = query
                .bind::<Integer, _>(self.package_id)
                .bind::<SmallInt, _>(self.major)
                .bind::<SmallInt, _>(self.minor)
                .bind::<SmallInt, _>(self.patch);

            query.load::<ScoreAndCount>(conn)?
        };

        let res_used = {
            let query = diesel::sql_query(
                "SELECT count(DISTINCT package_version_reviews.user_id) as count \
                 FROM package_versions \
                 INNER JOIN package_version_reviews \
                 ON package_versions.id =
                 package_version_reviews.package_version_id \
                 WHERE \
                 package_versions.package_id = $1 \
                 AND package_versions.major = $2 \
                 AND package_versions.minor = $3 \
                 AND package_versions.patch <= $4 \
                 AND package_versions.pre = FALSE \
                 AND package_version_reviews.score <> $5 \
                 ",
            );
            let query = query
                .bind::<Integer, _>(self.package_id)
                .bind::<SmallInt, _>(self.major)
                .bind::<SmallInt, _>(self.minor)
                .bind::<SmallInt, _>(self.patch)
                .bind::<SmallInt, _>(ReviewScore::None.to_i16().unwrap());

            query.load::<ScoreAndCount>(conn)?
        };

        let mut res: BTreeMap<_, _> = res.iter()
            .cloned()
            .map(|sc| (ReviewScore::from_i16_default(sc.0), sc.1))
            .collect();

        res.insert(ReviewScore::Used, res_used[0].1);
        Ok(res)
    }

    pub(crate) fn calculate_stats_major(
        &self,
        conn: &PgConnection,
    ) -> Result<BTreeMap<ReviewScore, i64>> {
        let res = {
            let query = diesel::sql_query(
                "SELECT score, count(DISTINCT package_version_reviews.user_id) as count \
                 FROM package_versions \
                 INNER JOIN package_version_reviews \
                 ON package_versions.id =
                 package_version_reviews.package_version_id \
                 WHERE \
                 package_versions.package_id = $1 \
                 AND package_versions.major = $2 \
                 AND ( \
                 package_versions.minor < $3 \
                 OR ( \
                 package_versions.minor = $3 \
                 AND package_versions.patch <= $4 \
                 ) \
                 ) \
                 AND package_versions.pre = FALSE \
                 GROUP BY score",
            );
            let query = query
                .bind::<Integer, _>(self.package_id)
                .bind::<SmallInt, _>(self.major)
                .bind::<SmallInt, _>(self.minor)
                .bind::<SmallInt, _>(self.patch);

            query.load::<ScoreAndCount>(conn)?
        };

        let res_used = {
            let query = diesel::sql_query(
                "SELECT count(DISTINCT package_version_reviews.user_id) as count \
                 FROM package_versions \
                 INNER JOIN package_version_reviews \
                 ON package_versions.id =
                 package_version_reviews.package_version_id \
                 WHERE \
                 package_versions.package_id = $1 \
                 AND package_versions.major = $2 \
                 AND ( \
                 package_versions.minor < $3 \
                 OR ( \
                 package_versions.minor = $3 \
                 AND package_versions.patch <= $4 \
                 ) \
                 ) \
                 AND package_versions.pre = FALSE \
                 AND package_version_reviews.score <> $5 \
                 ",
            );
            let query = query
                .bind::<Integer, _>(self.package_id)
                .bind::<SmallInt, _>(self.major)
                .bind::<SmallInt, _>(self.minor)
                .bind::<SmallInt, _>(self.patch)
                .bind::<SmallInt, _>(ReviewScore::None.to_i16().unwrap());

            query.load::<ScoreAndCount>(conn)?
        };
        let mut res: BTreeMap<_, _> = res.iter()
            .cloned()
            .map(|sc| (ReviewScore::from_i16_default(sc.0), sc.1))
            .collect();

        res.insert(ReviewScore::Used, res_used[0].1);
        Ok(res)
    }

    pub(crate) fn calculate_stats_total(
        &self,
        conn: &PgConnection,
    ) -> Result<BTreeMap<ReviewScore, i64>> {
        let res = {
            let query = diesel::sql_query(
                "SELECT score, count(DISTINCT package_version_reviews.user_id) as count \
                 FROM package_versions \
                 INNER JOIN package_version_reviews \
                 ON package_versions.id =
                 package_version_reviews.package_version_id \
                 WHERE \
                 package_versions.package_id = $1 \
                 GROUP BY score",
            );
            let query = query.bind::<Integer, _>(self.package_id);

            query.load::<ScoreAndCount>(conn)?
        };
        let res_used = {
            let query = diesel::sql_query(
                "SELECT count(DISTINCT package_version_reviews.user_id) as count \
                 FROM package_versions \
                 INNER JOIN package_version_reviews \
                 ON package_versions.id =
                 package_version_reviews.package_version_id \
                 WHERE \
                 package_versions.package_id = $1 \
                 AND package_version_reviews.score <> $2 \
                 ",
            );
            let query = query
                .bind::<Integer, _>(self.package_id)
                .bind::<SmallInt, _>(ReviewScore::None.to_i16().unwrap());

            query.load::<ScoreAndCount>(conn)?
        };
        let mut res: BTreeMap<_, _> = res.iter()
            .cloned()
            .map(|sc| (ReviewScore::from_i16_default(sc.0), sc.1))
            .collect();

        res.insert(ReviewScore::Used, res_used[0].1);
        Ok(res)
    }
}

// }}}

// {{{ package version reviews
#[derive(Debug, Queryable, Serialize, Associations)]
#[table_name = "package_version_reviews"]
#[belongs_to(PackageVersion)]
pub struct PackageVersionReview {
    pub user_id: i32,
    pub package_version_id: i64,
    pub score: i16,
    pub comment: String,
    pub created_at: chrono::NaiveDateTime,
    pub updated_at: chrono::NaiveDateTime,
}

#[derive(Insertable)]
#[table_name = "package_version_reviews"]
pub struct NewPackageVersionReview<'a> {
    pub user_id: i32,
    pub package_version_id: i64,
    pub score: i16,
    pub comment: &'a str,
    pub updated_at: chrono::NaiveDateTime,
}

pub struct ReviewData {
    pub user_id: UserId,
    pub username: String,
    pub user_trust_count: UserId,
    pub source_id: SourceId,
    pub name: String,
    pub version: SimpleSemver,
    pub score: ReviewScore,
    pub comment: String,
}

impl PackageVersionReview {
    pub fn create_or_update(
        user_id: i32,
        package_version_id: i64,
        score: i16,
        comment: &str,
        conn: &PgConnection,
    ) -> QueryResult<PackageVersionReview> {
        let now = chrono::Utc::now().naive_utc();
        let new = NewPackageVersionReview {
            user_id: user_id,
            package_version_id: package_version_id,
            score: score,
            comment: comment,
            updated_at: now,
        };

        diesel::insert_into(package_version_reviews::table)
            .values(&new)
            .on_conflict((
                package_version_reviews::user_id,
                package_version_reviews::package_version_id,
            ))
            .do_update()
            .set((
                package_version_reviews::score.eq(score),
                package_version_reviews::comment.eq(comment),
                package_version_reviews::updated_at.eq(now),
            ))
            .get_result(conn)
    }

    pub fn create_if_does_not_exist(
        user_id: i32,
        package_version_id: i64,
        score: i16,
        comment: &str,
        conn: &PgConnection,
    ) -> QueryResult<Option<PackageVersionReview>> {
        let now = chrono::Utc::now().naive_utc();
        let new = NewPackageVersionReview {
            user_id: user_id,
            package_version_id: package_version_id,
            score: score,
            comment: comment,
            updated_at: now,
        };

        diesel::insert_into(package_version_reviews::table)
            .values(&new)
            .on_conflict_do_nothing()
            .get_result(conn)
            .optional()
    }

    pub(crate) fn load(
        user_id: i32,
        package_version_id: i64,
        conn: &PgConnection,
    ) -> Result<Option<PackageVersionReview>> {
        let res: Option<PackageVersionReview> = package_version_reviews::table
            .filter(package_version_reviews::user_id.eq(user_id))
            .filter(package_version_reviews::package_version_id.eq(package_version_id))
            .first(conn)
            .optional()?;
        Ok(res)
    }

    pub(crate) fn load_by_username(name: &str, conn: &PgConnection) -> Result<Vec<ReviewData>> {
        let user = User::load_by_username(name, conn)?.ok_or(NotFound)?;
        let list: Vec<
            (
                i16,
                String,
                i16,
                String,
                UserId,
                i16,
                i16,
                i16,
                bool,
                UserId,
                String,
                i32,
            ),
        > = package_version_reviews::table
            .inner_join(users::table)
            .inner_join(package_versions::table.inner_join(packages::table))
            .select((
                packages::source,
                packages::name,
                package_version_reviews::score,
                package_version_reviews::comment,
                package_version_reviews::user_id,
                package_versions::major,
                package_versions::minor,
                package_versions::patch,
                package_versions::pre,
                users::id,
                users::username,
                users::trust_dst_count,
            ))
            .filter(users::id.eq(user.id))
            .load(conn)?;

        let list = list.into_iter()
            .map(|s| ReviewData {
                source_id: SourceId::from_i16_default(s.0),
                name: s.1,
                version: SimpleSemver {
                    major: s.5,
                    minor: s.6,
                    patch: s.7,
                    pre: s.8,
                }.into(),
                score: ReviewScore::from_i16_default(s.2),
                comment: s.3,
                user_id: s.9,
                username: s.10,
                user_trust_count: s.11,
            })
            .collect();

        Ok(list)
    }
}

// }}}

// {{{ user trust
#[derive(Queryable, Serialize)]
#[table_name = "user_trusts"]
pub struct UserTrust {
    pub src_user_id: UserId,
    pub dst_user_id: UserId,
    pub created_at: chrono::NaiveDateTime,
}

#[derive(Insertable)]
#[table_name = "user_trusts"]
pub struct NewUserTrust {
    pub src_user_id: UserId,
    pub dst_user_id: UserId,
}

impl UserTrust {
    pub fn set_trust(src: UserId, dst: UserId, conn: &PgConnection) -> Result<()> {
        let new = NewUserTrust {
            src_user_id: src,
            dst_user_id: dst,
        };

        conn.transaction(|| -> Result<()> {
            if let Some(user_trust) = diesel::insert_into(user_trusts::table)
                .values(&new)
                .on_conflict_do_nothing()
                .get_result::<UserTrust>(conn)
                .optional()?
            {
                assert_eq!(user_trust.dst_user_id, dst);

                diesel::update(users::table.filter(users::id.eq(user_trust.dst_user_id)))
                    .set(users::trust_dst_count.eq(users::trust_dst_count + 1))
                    .get_result::<User>(conn)
                    .optional()?;
            }

            Ok(())
        })
    }

    pub fn unset_trust(src: UserId, dst: UserId, conn: &PgConnection) -> Result<()> {
        conn.transaction(|| -> Result<()> {
            if let Some(user_trust) = diesel::delete(
                user_trusts::table
                    .filter(
                        user_trusts::src_user_id.eq(src),
                        //user_trusts::dst_user_id.eq(dst),
                    )
                    .filter(user_trusts::dst_user_id.eq(dst)),
            ).get_result::<UserTrust>(conn)
                .optional()?
            {
                assert_eq!(user_trust.dst_user_id, dst);

                diesel::update(users::table.filter(users::id.eq(user_trust.dst_user_id)))
                    .set(users::trust_dst_count.eq(users::trust_dst_count - 1))
                    .get_result::<User>(conn)
                    .optional()?;
            }

            Ok(())
        })
    }

    pub(crate) fn exists(src: UserId, dst: UserId, conn: &PgConnection) -> Result<bool> {
        Ok(user_trusts::table
            .filter(user_trusts::src_user_id.eq(src))
            .filter(user_trusts::dst_user_id.eq(dst))
            .first::<UserTrust>(conn)
            .optional()?
            .is_some())
    }
}

// }}}

// {{{ user flag
#[derive(Queryable, Serialize)]
#[table_name = "user_flags"]
pub struct UserFlag {
    pub src_user_id: UserId,
    pub dst_user_id: UserId,
    pub created_at: chrono::NaiveDateTime,
}

#[derive(Insertable)]
#[table_name = "user_flags"]
pub struct NewUserFlag {
    pub src_user_id: UserId,
    pub dst_user_id: UserId,
}

impl UserFlag {
    pub fn set_flag(src: UserId, dst: UserId, conn: &PgConnection) -> Result<()> {
        let new = NewUserFlag {
            src_user_id: src,
            dst_user_id: dst,
        };

        conn.transaction(|| -> Result<()> {
            if let Some(user_flag) = diesel::insert_into(user_flags::table)
                .values(&new)
                .on_conflict_do_nothing()
                .get_result::<UserTrust>(conn)
                .optional()?
            {
                assert_eq!(user_flag.dst_user_id, dst);

                diesel::update(users::table.filter(users::id.eq(user_flag.dst_user_id)))
                    .set(users::flag_dst_count.eq(users::flag_dst_count + 1))
                    .get_result::<User>(conn)
                    .optional()?;
            }

            Ok(())
        })
    }

    pub fn unset_flag(src: UserId, dst: UserId, conn: &PgConnection) -> Result<()> {
        conn.transaction(|| -> Result<()> {
            if let Some(user_flag) = diesel::delete(
                user_flags::table
                    .filter(
                        user_flags::src_user_id.eq(src),
                        //user_flags::dst_user_id.eq(dst),
                    )
                    .filter(user_flags::dst_user_id.eq(dst)),
            ).get_result::<UserTrust>(conn)
                .optional()?
            {
                assert_eq!(user_flag.dst_user_id, dst);

                diesel::update(users::table.filter(users::id.eq(user_flag.dst_user_id)))
                    .set(users::flag_dst_count.eq(users::flag_dst_count - 1))
                    .get_result::<User>(conn)
                    .optional()?;
            }

            Ok(())
        })
    }

    pub(crate) fn exists(src: UserId, dst: UserId, conn: &PgConnection) -> Result<bool> {
        Ok(user_flags::table
            .filter(user_flags::src_user_id.eq(src))
            .filter(user_flags::dst_user_id.eq(dst))
            .first::<UserTrust>(conn)
            .optional()?
            .is_some())
    }
}

// }}}

// {{{ package counts
#[derive(Queryable, Serialize, Insertable)]
#[table_name = "pkgver_total_counts"]
#[belongs_to(Package)]
pub struct PkgVerTotalCounts {
    pub package_id: PkgId,
    pub good: Count,
    pub bad: Count,
    pub used: Count,
}

impl PkgVerTotalCounts {
    pub fn create_or_update(
        package_id: PkgId,
        counts: data::Counts,
        conn: &PgConnection,
    ) -> QueryResult<PkgVerTotalCounts> {
        let new = PkgVerTotalCounts {
            package_id: package_id,
            good: counts.good,
            bad: counts.bad,
            used: counts.used,
        };

        diesel::insert_into(pkgver_total_counts::table)
            .values(&new)
            .on_conflict(pkgver_total_counts::package_id)
            .do_update()
            .set((
                pkgver_total_counts::good.eq(counts.good),
                pkgver_total_counts::bad.eq(counts.bad),
                pkgver_total_counts::used.eq(counts.used),
            ))
            .get_result(conn)
    }
}

#[derive(Queryable, Serialize, Insertable)]
#[table_name = "pkgver_major_counts"]
#[belongs_to(Package)]
pub struct PkgVerMajorCounts {
    pub package_id: PkgId,
    pub major: i16,
    pub good: Count,
    pub bad: Count,
    pub used: Count,
}

impl PkgVerMajorCounts {
    pub fn create_or_update(
        package_id: PkgId,
        major: i16,
        counts: data::Counts,
        conn: &PgConnection,
    ) -> QueryResult<PkgVerMajorCounts> {
        let new = PkgVerMajorCounts {
            package_id: package_id,
            major: major,
            good: counts.good,
            bad: counts.bad,
            used: counts.used,
        };

        diesel::insert_into(pkgver_major_counts::table)
            .values(&new)
            .on_conflict((pkgver_major_counts::package_id, pkgver_major_counts::major))
            .do_update()
            .set((
                pkgver_major_counts::good.eq(counts.good),
                pkgver_major_counts::bad.eq(counts.bad),
                pkgver_major_counts::used.eq(counts.used),
            ))
            .get_result(conn)
    }
}

#[derive(Queryable, Serialize, Insertable)]
#[table_name = "pkgver_minor_counts"]
#[belongs_to(Package)]
pub struct PkgVerMinorCounts {
    pub package_id: PkgId,
    pub major: i16,
    pub minor: i16,
    pub good: Count,
    pub bad: Count,
    pub used: Count,
}

impl PkgVerMinorCounts {
    pub fn create_or_update(
        package_id: PkgId,
        major: i16,
        minor: i16,
        counts: data::Counts,
        conn: &PgConnection,
    ) -> QueryResult<PkgVerMinorCounts> {
        let new = PkgVerMinorCounts {
            package_id: package_id,
            major: major,
            minor: minor,
            good: counts.good,
            bad: counts.bad,
            used: counts.used,
        };

        diesel::insert_into(pkgver_minor_counts::table)
            .values(&new)
            .on_conflict((
                pkgver_minor_counts::package_id,
                pkgver_minor_counts::major,
                pkgver_minor_counts::minor,
            ))
            .do_update()
            .set((
                pkgver_minor_counts::good.eq(counts.good),
                pkgver_minor_counts::bad.eq(counts.bad),
                pkgver_minor_counts::used.eq(counts.used),
            ))
            .get_result(conn)
    }
}

#[derive(Queryable, Serialize, Insertable)]
#[table_name = "pkgver_patch_counts"]
#[belongs_to(Package)]
pub struct PkgVerPatchCounts {
    pub package_id: PkgId,
    pub major: i16,
    pub minor: i16,
    pub patch: i16,
    pub pre: bool,
    pub good: Count,
    pub bad: Count,
    pub used: Count,
    pub review_ratio_cents: i64,
}

impl PkgVerPatchCounts {
    pub fn create_or_update(
        package_id: PkgId,
        major: i16,
        minor: i16,
        patch: i16,
        pre: bool,
        counts: data::Counts,
        conn: &PgConnection,
    ) -> QueryResult<PkgVerPatchCounts> {
        let new = PkgVerPatchCounts {
            package_id: package_id,
            major: major,
            minor: minor,
            patch: patch,
            pre: pre,
            good: counts.good,
            bad: counts.bad,
            used: counts.used,
            review_ratio_cents: 100 * (counts.used) / (counts.good + counts.bad + 1),
        };

        diesel::insert_into(pkgver_patch_counts::table)
            .values(&new)
            .on_conflict((
                pkgver_patch_counts::package_id,
                pkgver_patch_counts::major,
                pkgver_patch_counts::minor,
                pkgver_patch_counts::patch,
                pkgver_patch_counts::pre,
            ))
            .do_update()
            .set((
                pkgver_patch_counts::good.eq(counts.good),
                pkgver_patch_counts::bad.eq(counts.bad),
                pkgver_patch_counts::used.eq(counts.used),
                pkgver_patch_counts::review_ratio_cents.eq(new.review_ratio_cents),
            ))
            .get_result(conn)
    }
}
// }}}
// vim: foldmethod=marker foldmarker={{{,}}}
