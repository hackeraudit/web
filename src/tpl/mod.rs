pub mod home;
pub mod base;
pub mod misc;
pub mod unauthorized;
pub mod user;
pub mod search;
pub mod top;
pub mod security;
pub mod pkg;
pub mod pkgver;
pub mod review;
pub mod stats;
pub mod register;

use failure::{self, Fail};
use serde;
use std;
use std::path::{Path, PathBuf};
use stpl;
use stpl::{Template, TemplateExt};
use stpl::html;

pub fn home_tpl() -> impl Template<Argument = ::tpl::home::Data> {
    html::Template::new("home", ::tpl::home::page)
}

pub fn pkg_tpl() -> impl Template<Argument = ::tpl::pkg::Data> {
    html::Template::new("pkg", ::tpl::pkg::page)
}

pub fn pkgver_tpl() -> impl Template<Argument = ::tpl::pkgver::Data> {
    html::Template::new("pkgver", ::tpl::pkgver::page)
}
pub fn top_wanted_tpl() -> impl Template<Argument = ::tpl::top::wanted::Data> {
    html::Template::new("top_wanted", ::tpl::top::wanted::page)
}

pub fn top_users_tpl() -> impl Template<Argument = ::tpl::top::users::Data> {
    html::Template::new("top_users", ::tpl::top::users::page)
}

pub fn unauthorized_tpl() -> impl Template<Argument = ::tpl::unauthorized::Data> {
    html::Template::new("unauthorized", ::tpl::unauthorized::page)
}

pub fn register_tpl() -> impl Template<Argument = ::tpl::register::Data> {
    html::Template::new("register", ::tpl::register::page)
}

pub fn user_tpl() -> impl Template<Argument = ::tpl::user::Data> {
    html::Template::new("user", ::tpl::user::page)
}

pub fn security_tpl() -> impl Template<Argument = ::tpl::security::Data> {
    html::Template::new("self_security", ::tpl::security::page)
}
pub fn search_tpl() -> impl Template<Argument = ::tpl::search::Data> {
    html::Template::new("search", ::tpl::search::page)
}

#[derive(Debug)]
pub struct Rendered(pub(crate) Vec<u8>);

pub fn render<T: stpl::Template>(template: &T, data: &<T as Template>::Argument) -> Rendered
where
    <T as Template>::Argument: serde::Serialize + 'static,
{
    let path = std::env::args_os().next().unwrap();
    let path: &Path = path.as_ref();
    let mut path: PathBuf = path.to_path_buf();
    path.set_file_name("template");

    Rendered(template.render_dynamic(&path, data).unwrap_or_else(|e| {
        eprintln!("Rendering template {} failed: {}", template.key(), e);
        if let stpl::DynamicError::Failed {
            ref stderr,
            ref stdout,
            ..
        } = e
        {
            let b = failure::Backtrace::new();
            eprintln!(
                "stdout: {}\nstderr: {}\nbacktrace:{}\n",
                String::from_utf8_lossy(&stdout),
                String::from_utf8_lossy(&stderr),
                e.backtrace().unwrap_or_else(|| &b),
            );
        }
        "Internal error".into()
    }))
}
