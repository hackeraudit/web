use stpl::Render;
use stpl::html::*;

use super::misc;

use hackeraudit_api_common;

#[derive(Serialize, Deserialize, Clone)]
pub struct Data {
    pub short_stats: bool,
    pub stats: hackeraudit_api_common::UsageStats,
}

pub fn render(data: &Data) -> impl Render {
    ((
            h4("Community stats"),
            table.class("table")((
                    thead((
                            tr((
                                    th.scope("col")(
                                        div
                                        .class("text-center")
                                        .data_toggle("tooltip")
                                        .data_placement("top")
                                        .title("Hover over column and row titles for more information")
                                        (
                                            misc::img_svg_icon("info-with-circle", false)
                                        )
                                        ),
                                        th.scope("col").class("text-right")(
                                            span.data_toggle("tooltip").data_placement("top")
                                            .title("Number of users who rated as BAD...")(
                                                "Bad")),
                                                th.scope("col").class("text-right")(
                                                    span.
                                                    data_toggle("tooltip").data_placement("top").
                                                    title("Number of user who rated as GOOD...")(
                                                        "Good")),
                                                        th.scope("col").class("text-right")(
                                                            span.data_toggle("tooltip").data_placement("top")
                                                            .title("Number of users who used or rated...")(
                                                                "Used")),
                                                                ))
                                            )),
                                            tbody((
                                                    if !data.short_stats {
                                                        Some((tr((
                                                                    th.scope("row")(
                                                                        span
                                                                        .data_toggle("tooltip").data_placement("top")
                                                                        .title(
                                                                            "... this exact patch version")("Patch")
                                                                        ),
                                                                        td.class("text-right")(data.stats.patch.bad),
                                                                        td.class("text-right")(data.stats.patch.good),
                                                                        td.class("text-right")(data.stats.patch.used),
                                                                        )),

                                                            tr((
                                                            th.scope("row")(
                                                            span
                                                            .data_toggle("tooltip").data_placement("top")
                                                            .title("... this minor version in this or lower patch version")(
                                                            "Minor")),
                                                            td.class("text-right")(data.stats.minor.bad),
                                                            td.class("text-right")(data.stats.minor.good),
                                                            td.class("text-right")(data.stats.minor.used),
                                                            )),
                                                            tr((
                                                            th.scope("row")(
                                                            span.
                                                            data_toggle("tooltip").data_placement("top")
                                                            .title("... this major version in this or lower major&minor version")(
                                                            "Major")),
                                                            td.class("text-right")(data.stats.major.bad),
                                                            td.class("text-right")(data.stats.major.good),
                                                            td.class("text-right")(data.stats.major.used),
                                                            )),
                                                   ))
                                                    } else {
                                                        None
                                                    },
                                                    tr((
                                                            th.scope("row")(
                                                            span.
                                                            data_toggle("tooltip").data_placement("top")
                                                            .title("... any version")(
                                                            "All")),
                                                            td.class("text-right")(data.stats.total.bad),
                                                            td.class("text-right")(data.stats.total.good),
                                                            td.class("text-right")(data.stats.total.used)
                                                            ))


    ))
        ))
        ))
}
