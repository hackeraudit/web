use super::base;
use super::misc;
use super::misc::*;
use super::review;
use super::stats;
use data::{SimpleSemver, SourceId};
use data::ReviewScore;
use stpl::Render;
use stpl::html::*;

#[derive(Serialize, Deserialize, Clone)]
pub struct Review {
    pub score: ReviewScore,
    pub comment: String,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Data {
    pub base: base::Data,
    pub info_url: String,
    pub download_url: String,
    pub source_id: SourceId,
    pub pkg_name: String,
    pub pkg_version: SimpleSemver,
    pub reviews: Vec<review::Data>,
    pub stats: stats::Data,
    pub review: Option<Review>,
}

pub fn page(data: &Data) -> impl Render {
    let score = data.review.as_ref().map(|r| r.score);
    let form_url = format!(
        "/package/{}/{}/{}/review",
        data.source_id.to_string(),
        data.pkg_name.clone(),
        data.pkg_version.to_string()
    );
    let content = (
        misc::breadcrumb(vec![
            "Package".into(),
            data.source_id.to_string().into(),
            a_pkg(data.source_id, &data.pkg_name).into(),
            data.pkg_version.to_string().into(),
        ]),
        row((
            col((
                h4("Package information"),
                ul((
                    li(a.href(data.info_url.clone())((
                        img_svg_icon("link", false),
                        nbsp,
                        "More information",
                    ))),
                    li(a.href(data.download_url.clone())((
                        img_svg_icon("download", false),
                        nbsp,
                        "Download package source",
                    ))),
                )),
                stats::render(&data.stats),
            )),
            col((form.action(form_url).method("post")(
                (div.class("form-group").id("reviewResultSelectGroup")((
                    h4((label.for_("reviewResultSelectGroup")("My review"))),
                    if let Some(ref _session) = data.base.session {
                        (
                            Some((
                                (vec![
                                    (ReviewScore::None, "none", (Some("I haven't used"), None)),
                                    (
                                        ReviewScore::Used,
                                        "used",
                                        (Some("I've used but haven't checked"), None),
                                    ),
                                    (
                                        ReviewScore::Bad,
                                        "bad",
                                        (None, Some(("I've checked and it's ", i("BAD")))),
                                    ),
                                    (
                                        ReviewScore::Good,
                                        "good",
                                        (None, Some(("I've checked and it's ", i("GOOD")))),
                                    ),
                                ].into_iter()
                                    .map(|(s, s_str, t)| {
                                        div.class("form-check")(label.class("form-check-label")((
                                            {
                                                let mut i1 = input
                                                    .class("form-check-input")
                                                    .type_("radio")
                                                    .name("score")
                                                    .id(format!("option-{}", s_str))
                                                    .value(s_str);
                                                if score == Some(s) {
                                                    i1 = i1.checked()
                                                }
                                                i1
                                            },
                                            t,
                                        )))
                                    })
                                    .collect::<Vec<_>>()),
                                /*
                   <div class="form-check">
                   <label class="form-check-label">
                   <input class="form-check-input" type="radio" name="score" id="option-used" value="used"
                   {% if template_data.review.score == "used" %}checked{% endif %}
                   >
                   I've used but haven't checked
                   </label>
                   </div>
                   <div class="form-check">
                   <label class="form-check-label">
                   <input class="form-check-input" type="radio" name="score" id="option-bad" value="bad"
                   {% if template_data.review.score == "bad" %}checked{% endif %}
                   >
                   I've checked and it's <i>BAD</i> (avoid)
                   </label>
                   </div>
                   <div class="form-check">
                   <label class="form-check-label">
                   <input class="form-check-input" type="radio" name="score" id="option-good" value="good"
                   {% if template_data.review.score == "good" %}checked{% endif %}
                   >
                   I've checked and it's <i>GOOD</i> (I recommend)
                   </label>
                   </div>
                   */
                                div.class("form-group")((
                                    label.for_("score-comment")("Details"),
                                    {
                                        let mut ta = textarea.
                                class("form-control")
                                .id("score-comment")
                                .name("comment")
                                .rows("3")
                                .placeholder("How extensive was the review, good vs bad parts...");

                                        if score == Some(ReviewScore::None)
                                            || score == Some(ReviewScore::Used)
                                        {
                                            ta = ta.disabled()
                                        }

                                        ta(data.review.as_ref().map(|r| r.comment.clone()))
                                    },
                                )),
                                button
                                    .id("review-submit")
                                    .type_("submit")
                                    .class("btn btn-primary")("Post"),
                            )),
                            None,
                        )
                    } else {
                        (
                            None,
                            Some(div.class("alert alert-info").role("alert")(
                                "Sign-in to view and edit your review!",
                            )),
                        )
                    },
                ))),
            ))),
        )),
        div.class("row justify-content-center")(h3("Reviews")),
        review::list(&data.reviews, review::ListMode::Package),
    );

    base::base_with_js(
        &data.base,
        Box::new(content),
        Box::new(script.type_("text/javascript")(raw(COMMENT_DISABLED_JS))),
    )
}

const COMMENT_DISABLED_JS: &str = include_str!("comment_disabled.js");
