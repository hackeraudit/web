use super::base::Data;
use data::*;
use std::env;
use stpl::Render;
use stpl::html::*;

pub fn flash(data: &Data) -> (impl Render, impl Render) {
    (
        div.class("container")(data.flash.as_ref().map(|flash| {
            div.id("flash").class(format!(
                "alert alert-{} mt-2",
                if flash.is_error { "danger" } else { "info" }
            ))(flash.msg.clone())
        })),
        script.type_("text/javascript")(raw(FLASH_SCRIPT)),
    )
}

pub fn session_menu(data: &Data) -> impl Render {
    use stpl::html::*;

    div.id("session-menu")(if let Some(ref session) = data.session {
        Box::new(div.class("dropdown")((
            button
                .href("#")
                .class("btn btn-success dropdown-toggle")
                .data_toggle("dropdown")
                .role("button")
                .aria_expanded("false")
                .aria_haspopup("true")((img_svg_icon("tools", true), "Menu")),
            div.class("dropdown-menu")
                .aria_labelledby("meSession")
                .role("menu")((
                a.class("dropdown-item")
                    .href(user_url(session.username.as_str()))((
                    img_svg_icon("user", false),
                    "Profile",
                )),
                a.class("dropdown-item").href("/self/security")((
                    img_svg_icon("fingerprint", false),
                    "Security",
                )),
                a.class("dropdown-item").href("/auth/logout")((
                    img_svg_icon("log-out", false),
                    "Logout",
                )),
            )),
        ))) as Box<Render>
    } else {
        Box::new(
                form
                     .class("navbar-left btn-group").role("group")((
                a.href(format!("/oauth/github/login?src={}", data.current_url))
                    .class("btn btn-success")((
                    img_svg_icon("login", true),
                    nbsp,
                    "Sign-in",
                )),
                a.role("button")
                    .class("btn btn-success dropdown-toggle navbar-btn")
                    .href("#")
                    .data_toggle("dropdown")
                    .aria_expanded("false"),
                div.class("dropdown-menu").role("menu")(
                    a.class("dropdown-item")
                        .href(format!("/oauth/github/login?src={}", data.current_url))((
                    img_svg_icon_social("github", false),
                    nbsp,
                    "with Github",
                    ))
                ),
            ))) as Box<Render>
    })
}

pub fn img_svg_icon_src(name: &str) -> String {
    format!("/static/theme/Entypo+/Entypo+/{}.svg", name)
}

pub fn img_svg_icon(name: &'static str, white: bool) -> impl Render {
    img.class(if white {
        "svg icon icon-white"
    } else {
        "svg icon"
    }).src(img_svg_icon_src(name))
        .alt(name)
}

pub fn img_svg_miniicon(name: &'static str, white: bool) -> impl Render {
    img.class(if white {
        "svg icon mini-icon icon-white"
    } else {
        "svg mini-icom icon"
    }).src(img_svg_icon_src(name))
        .alt(name)
}

pub fn img_svg_icon_social_src(name: &str) -> String {
    format!(
        "/static/theme/Entypo+/Entypo+ Social Extension/{}.svg",
        name
    )
}

pub fn img_svg_icon_social(name: &'static str, white: bool) -> impl Render {
    img.class(if white {
        "svg icon icon-white"
    } else {
        "svg icon"
    }).src(img_svg_icon_social_src(name))
        .alt(name)
}

pub struct BreadCrumbItem(Box<Render>);

impl<T: Render + 'static> From<T> for BreadCrumbItem {
    fn from(t: T) -> BreadCrumbItem {
        BreadCrumbItem(Box::new(t))
    }
}

pub fn breadcrumb(mut names: Vec<BreadCrumbItem>) -> impl Render {
    let names_len = names.len();

    nav.aria_label("breadcrumb").role("navigation")(ol.class("breadcrumb")(
        (names
            .drain(..)
            .enumerate()
            .map(|(n, render)| {
                if n + 1 != names_len {
                    Box::new(li.class("breadcrumb-item")(render.0)) as Box<Render>
                } else {
                    Box::new(li.class("breadcrumb-item active").aria_current("page")(
                        render.0,
                    )) as Box<Render>
                }
            })
            .collect::<Vec<Box<Render>>>()),
    ))
}

pub fn id_user_review(source_id: SourceId, package_name: &str, version: SimpleSemver) -> String {
    format!(
        "review-{}-{}-{}",
        source_id.to_str(),
        package_name,
        version.to_string()
    )
}

pub fn url_pkg_version(source_id: SourceId, package_name: &str, version: SimpleSemver) -> String {
    format!(
        "/package/{}/{}/{}",
        source_id.to_str(),
        package_name,
        version.to_string(),
    )
}

pub fn url_base() -> String {
    env::var("BASE_URL").expect("BASE_URL must be set")
}

pub fn url_pkg(source_id: SourceId, package_name: &str) -> String {
    format!("/package/{}/{}", source_id.to_str(), package_name,)
}

pub fn a_pkg(source_id: SourceId, package_name: &str) -> impl Render {
    a.href(url_pkg(source_id, package_name))(package_name.to_owned())
}

pub fn url_user_review(
    username: &str,
    source_id: SourceId,
    package_name: &str,
    version: SimpleSemver,
) -> String {
    format!(
        "/user/{}#{}",
        username,
        id_user_review(source_id, package_name, version)
    )
}
pub fn a_user(username: &str, trust_count: u32) -> impl Render {
    a.href(format!("/user/{}", username,))((
        username.to_string(),
        " ",
        user_trust_badge(trust_count),
    ))
}

pub fn user_trust_badge(count: u32) -> impl Render {
    span.class("badge-pill badge-info")((count, img_svg_miniicon("star", true)))
}

pub fn user_url(username: &str) -> String {
    format!("/user/{}", username)
}

pub fn col<C: Render + 'static>(content: C) -> impl Render {
    div.class("col-sm mx-1 px-4")(content)
}

pub fn row<C: Render + 'static>(content: C) -> impl Render {
    div.class("row")(content)
}
const FLASH_SCRIPT: &str = include_str!("flash.js");
