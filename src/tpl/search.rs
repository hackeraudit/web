use stpl::Render;
use stpl::html::*;

use super::base;
use super::misc;

use data::SourceId;

use super::misc::col;

#[derive(Serialize, Deserialize, Clone)]
pub struct SearchResult {
    pub name: String,
    pub source_id: SourceId,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Data {
    pub base: base::Data,
    pub page: u64,
    pub total: u64,
    pub max: u64,
    pub total_pages: u64,
    pub query: String,
    pub results: Vec<SearchResult>,
}

pub fn url_search(query: &str, page: u64) -> String {
    format!("/package/search?q={}&p={}", query, page)
}
pub fn pagination(data: &Data) -> impl Render {
    div.class("row").style("max-width: 40em; margin: auto;")(
        nav.id("search-navigation")
            .style("margin-left: auto; margin-right: auto;")
            .aria_label("Page navigation")(ul.class("pagination")((
            if data.page >= 1 {
                li.class("page-item")(
                    a.class("page-link")
                        .href(url_search(&data.query, data.page - 1))(lt),
                )
            } else {
                li.class("page-item disabled")(a.class("page-link").href("#")(lt))
            },
            (-4..5)
                .map(|rel| {
                    let current = data.page as i32 + rel;
                    if current >= 0 && (current as u64) < data.total_pages {
                        Box::new({
                            li.class(format!(
                                    "page-item fixed-size {}",
                                    if current as u64 == data.page {
                                        "active"
                                    } else if rel < -2 || rel > 2 {
                                            "d-none d-md-block"
                                        } else {
                                            "d-none d-sm-block"
                                        }
                                ))(
                                    a.class("page-link").href(url_search(&data.query, current as u64))(
                                        current,
                                    ),
                                )
                        }) as Box<Render>
                    } else {
                        Box::new({
                            li.class("page-item disabled fixed-size d-none d-md-block")(
                                a.class("page-link").href("#")(nbsp),
                            )
                        }) as Box<Render>
                    }
                })
                .collect::<Vec<_>>(),
            if data.page + 1 < data.total_pages {
                li.class("page-item")(
                    a.class("page-link")
                        .href(url_search(&data.query, data.page + 1))(gt),
                )
            } else {
                li.class("page-item disabled")(a.class("page-link hidden-sm-down").href("#")(gt))
            },
        ))),
    )
}

pub fn page(data: &Data) -> impl Render {
    let content = (
        misc::breadcrumb(vec!["Package".into(), "Search".into()]),
        div.class("row")((
            col((
                h3("Results"),
                p(format!(
                    r##"
                    Found {} {} matching results.
                    "##,
                    if data.total == data.max {
                        "more than"
                    } else {
                        ""
                    },
                    data.total
                )),
            )),
            col(ul.class("list-group")((
                pagination(data),
                data.results
                    .iter()
                    .cloned()
                    .map(|pkg| {
                        a.class("list-group-item").href(format!(
                            "/package/{}/{}",
                            pkg.source_id.to_string(),
                            pkg.name,
                        ))((
                            pkg.name,
                            " (",
                            pkg.source_id.to_lang_string().to_string(),
                            ")",
                        ))
                    })
                    .collect::<Vec<_>>(),
                if data.total_pages == 0 {
                    Some(p.class("list-group-item")("No results"))
                } else {
                    None
                },
            ))),
        )),
    );

    base::base(&data.base, Box::new(content))
}
