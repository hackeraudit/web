$(document).ready(function() {
  $('input[type=radio][name=score]').change(function() {
    if ($(this).val() === 'none' || $(this).val() === 'used') {
      $("#score-comment").prop("disabled", true);
    } else {
      $("#score-comment").prop("disabled", false);
    }
  });


  $('textarea#score-comment').keydown(function (e) {
    if (e.keyCode === 13 && e.ctrlKey) {
      $('#review-submit').click();
      return false;
    }
  });
})
