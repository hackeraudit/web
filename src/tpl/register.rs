use stpl::Render;
use stpl::html::*;

use super::base;
use super::misc;

#[derive(Serialize, Deserialize, Clone)]
pub struct Data {
    pub username: String,
    pub base: base::Data,
}

pub fn page(data: &Data) -> impl Render {
    let content = (
        misc::breadcrumb(vec!["Register".into()]),
        h1.class("text-left")("Register"),
        form.role("form")
            .class("text-left")
            .method("post")
            .action("/register")((
            div.class("form-group")((
                label.for_("username")("Username"),
                input
                    .type_("username")
                    .name("username")
                    .class("form-control")
                    .id("username")
                    .placeholder("enter username")
                    .value(data.username.clone()),
            )),
            div.class("checkbox")((
                h2("Rules"),
                p("HackerAudit:"),
                ul((
                    li(
                        "only collects explicitly provided, or otherwise essential information and
        not spy on you in any way;",
                    ),
                    li("reserves a right to delete and block your account if you break any rules;"),
                    li("will provide better definition of what the rules are in the future."),
                )),
                p("You:"),
                ul((
                    li("will behave like a reasonable person;"),
                    li("won't abuse the system."),
                )),
                label(input.type_("checkbox").name("agree")((nbsp, "I accept"))),
            )),
            button.type_("submit").class("btn btn-primary")("Submit"),
        )),
    );
    base::base(&data.base, Box::new(content))
}
