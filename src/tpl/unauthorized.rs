use stpl::Render;
use stpl::html::*;

use super::base;

#[derive(Serialize, Deserialize, Clone)]
pub struct Data {
    pub base: base::Data,
}

pub fn page(data: &Data) -> impl Render {
    let content = div.class("row mt-2")
        .style("max-width: 40em; margin: auto;")(
        (div.class("col text-align-center")((
            h1("Not authorized"),
            p.class("lead")("Sign-in first"),
        ))),
    );
    base::base(&data.base, Box::new(content))
}
