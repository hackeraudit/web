use data;
use data::*;
use stpl::Render;
use stpl::html::*;

use super::misc::*;

#[derive(Serialize, Deserialize, Clone)]
pub struct Data {
    pub source_id: SourceId,
    pub pkg_name: String,
    pub pkg_version: SimpleSemver,
    pub score: ReviewScore,
    pub comment: String,
    pub username: String,
    pub user_trust_count: u32,
}

#[derive(Copy, Clone, PartialEq, Eq)]
pub enum ListMode {
    User,
    Package,
}

pub fn review(r: &Data, mode: ListMode) -> impl Render {
    (div.class("card mb-2")((
        if mode == ListMode::User {
            Some(div.class("card-header").id(id_user_review(
                r.source_id,
                &r.pkg_name,
                r.pkg_version,
            ))([
                a.href(url_pkg_version(r.source_id, &r.pkg_name, r.pkg_version))(format!(
                    "{}/{}:{}",
                    r.source_id.to_str(),
                    r.pkg_name,
                    r.pkg_version.to_string()
                )),
            ]))
        } else {
            None
        },
        div.class("card-body")(blockquote.class("blockquote nomargin")((
            if r.comment.is_empty() {
                None
            } else {
                Some(p.class("mb-0 card-text")(r.comment.to_string()))
            },
            if mode == ListMode::Package {
                Some(footer.class("blockquote-footer card-text")(
                    a.href(url_user_review(
                        &r.username,
                        r.source_id,
                        &r.pkg_name,
                        r.pkg_version,
                    ))((
                        r.username.clone(),
                        " ",
                        user_trust_badge(r.user_trust_count),
                    )),
                ))
            } else {
                None
            },
        ))),
    )))
}

pub fn column(
    reviews: &[Data],
    score: ::data::ReviewScore,
    name: &str,
    mode: ListMode,
) -> impl Render {
    let reviews = reviews
        .iter()
        .filter(|r| r.score == score)
        .map(|r| review(r, mode))
        .collect::<Vec<_>>();

    let no_reviews = reviews.is_empty();

    div.class("col")((
        h4.class("text-center")(name.to_string()),
        reviews,
        if no_reviews {
            Some(div.class("card mb-2")(div.class("card-body text-center")(
                p.class("card-text")("None yet"),
            )))
        } else {
            None
        },
    ))
}

pub fn list(reviews: &[Data], mode: ListMode) -> impl Render {
    div.class("row")((
        column(reviews, data::ReviewScore::Bad, "BAD", mode),
        column(reviews, data::ReviewScore::Good, "GOOD", mode),
    ))
}
