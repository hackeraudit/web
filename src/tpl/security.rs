use stpl::Render;
use stpl::html::*;

use super::{base, misc};

#[derive(Serialize, Deserialize, Clone)]
pub struct Data {
    pub base: base::Data,
    pub token: String,
}

pub fn page(data: &Data) -> impl Render {
    let content = (
        misc::breadcrumb(vec!["Self".into(), "Security".into()]),
        div.class("row")((
            misc::col((
                h3("Token"),
                p(r##"
                    Used to identify your API calls. Run the
                    following command to set your token:
                    "##),
                tt(("haudit token ", data.token.clone())),
            )),
            misc::col((
                label.for_("token-input").class("col-form-label")("Token"),
                span.class("form-inline input-group")((
                    span.id("token-input").class("form-control").type_("text")(data.token.clone()),
                    span.class("input-group-btn")(button
                        .onclick("copyToClipboard('token-input')")
                        .class("btn btn-info")(misc::img_svg_icon("copy", true))),
                )),
            )),
        )),
    );

    base::base_with_js(
        &data.base,
        Box::new(content),
        Box::new(script.type_("text/javascript")(raw(COPY_JS))),
    )
}

const COPY_JS: &str = include_str!("copy_to_clip.js");
