use stpl::Render;
use stpl::html::*;

use data::{SimpleSemver, SourceId};

use tpl::{base, misc};
use tpl::misc::{col, row};

#[derive(Serialize, Deserialize, Clone)]
pub struct Item {
    pub name: String,
    pub version: SimpleSemver,
    pub review_ratio: i64,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Data {
    pub base: base::Data,
    pub source_id: SourceId,
    pub all_time: Vec<Item>,
    pub fresh: Vec<Item>,
}

pub fn list(data: &Data, list: &[Item]) -> impl Render {
    ul.class("list-group")((list.iter()
        .cloned()
        .map(|pkg| {
            (a.class("list-group-item").href(format!(
                "/package/{}/{}/{}",
                data.source_id.to_string(),
                pkg.name,
                pkg.version.to_string(),
            ))((pkg.name, ":", pkg.version.to_string())),)
        })
        .collect::<Vec<_>>(),))
}

pub fn page(data: &Data) -> impl Render {
    let source = data.source_id.to_string();
    let content = (
        misc::breadcrumb(vec!["Top".into(), "Wanted".into(), source.into()]),
        row((
            col((
                h3("Top wanted"),
                p(r##"
                    The following packages have the highest users to reviews ratio.
                    Review any of them to make the biggest impact!
                    "##),
            )),
            col(list(data, &data.all_time)),
        )),
    );

    base::base(&data.base, Box::new(content))
}
