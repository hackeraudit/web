use stpl::Render;
use stpl::html::*;

use tpl::{base, misc};
use tpl::misc::{col, row};

#[derive(Serialize, Deserialize, Clone)]
pub struct Item {
    pub username: String,
    pub trust_count: u32,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Data {
    pub base: base::Data,
    pub users: Vec<Item>,
}

pub fn list(users: &[Item]) -> impl Render {
    ul.class("list-group")(
        users
            .iter()
            .map(|user| {
                li.class("list-group-item")(::tpl::misc::a_user(&user.username, user.trust_count))
            })
            .collect::<Vec<_>>(),
    )
}

pub fn page(data: &Data) -> impl Render {
    let content = (
        misc::breadcrumb(vec!["Top".into(), "Users".into()]),
        row((
            col((
                h3("Top users"),
                p(r##"
                    The following users are most trusted.
                    "##),
            )),
            col(list(&data.users)),
        )),
    );

    base::base(&data.base, Box::new(content))
}
