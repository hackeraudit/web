use super::base;
use super::misc;
use super::misc::*;
use super::review;
use super::stats;
use data::{SimpleSemver, SourceId};
use stpl::Render;
use stpl::html::*;

#[derive(Serialize, Deserialize, Clone)]
pub struct Data {
    pub base: base::Data,
    pub info_url: String,
    pub source_id: SourceId,
    pub last_pkg_version: SimpleSemver,
    pub pkg_name: String,
    pub reviews: Vec<review::Data>,
    pub versions: Vec<SimpleSemver>,
    pub stats: stats::Data,
}

pub fn page(data: &Data) -> impl Render {
    let content = (
        misc::breadcrumb(vec![
            "Package".into(),
            data.source_id.to_string().into(),
            data.pkg_name.clone().into(),
        ]),
        row((
            col((
                h4("Package information"),
                ul(li(a.href(data.info_url.clone())((
                    misc::img_svg_icon("link", false),
                    "More information",
                )))),
                stats::render(&data.stats),
            )),
            col((
                h4("Versions"),
                ul.class("list-group")(
                    (data.versions
                        .iter()
                        .map(|version| {
                            a.class("list-group-item").href(format!(
                                "/package/{}/{}/{}",
                                data.source_id.to_string(),
                                data.pkg_name.clone(),
                                version.to_string(),
                            ))(version.to_string())
                        })
                        .collect::<Vec<_>>()),
                ),
            )),
        )),
        div.class("row justify-content-center")(h3("Reviews")),
        review::list(&data.reviews, review::ListMode::Package),
    );
    base::base(&data.base, Box::new(content))
}
