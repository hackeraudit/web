use stpl::Render;
use stpl::html::*;

use super::{base, misc};
use super::misc::*;

#[derive(Serialize, Deserialize, Clone)]
pub struct Data {
    pub base: base::Data,
}

pub fn page(data: &Data) -> impl Render {
    let content = (
        misc::breadcrumb(vec!["Home".into()]),
        row((
            col((
                h1("Review all the things!"),
                    p.class("lead")("Community approach to trust in Open Source"),
                    p(r#"
            Most modern programming languages come integrated with a package
            manager. Examples include NPM, RubyGems, PIP, Cargo. Package
            managers improve code reuse and collaboration and truly change the
            way we build software."#),
                    p(
                r#" However, with every great power, comes a great responsibility.
            When your code depends on tens, hundreds or even thousands
            dependencies written by people you don't know, how do you make sure
            you can still trust it?
            "#,
                    ),
                    p(
                r#" The mission of Hacker Audit is preventing inclusion of hostile
            code in your software and increasing the quality and security of
            software everywhere! Package by package, release by release,
            together we can make sure that no code goes unchecked!
            "#,
                    ),
            )),
            col((
                h1("News"),
                h5("Getting started guide is ready"),
                p((
                    "Follow the ",
                    a.href("https://gitlab.com/hackeraudit/doc/wikis/Getting-started")("Getting started guide"),
                    " to start using Hacker Audit."
                )),
                h5("Initial realease"),
                p((
                    "Read the ",
                    a.href("https://gitlab.com/hackeraudit/doc/wikis/Hacker-Audit-v0.1-release-announcement")("v0.1 release announcement"),
                    "!"
                )),
           )),
       ))
    );

    base::base(&data.base, Box::new(content))
}
