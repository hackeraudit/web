use stpl::Render;
use stpl::html::*;

use super::base;
use super::misc;
use super::misc::*;

use super::review;

#[derive(Serialize, Deserialize, Clone)]
pub struct Data {
    pub username: String,
    pub base: base::Data,
    pub user_trust_count: u32,
    pub trusted: bool,
    pub flagged: bool,
    pub reviews: Vec<review::Data>,
}

pub fn post_button<S1, S2, S4>(url: S1, class: S2, icon: &'static str, text: S4) -> impl Render
where
    S1: Into<String>,
    S2: Into<String>,
    S4: Into<String>,
{
    form.action(url.into()).method("post")(button
        .id("review-submit")
        .type_("submit")
        .class(format!("btn {}", class.into()))((
        misc::img_svg_icon(icon, false),
        text.into(),
    )))
}

pub fn page(data: &Data) -> impl Render {
    let content = (
        misc::breadcrumb(vec!["Users".into(), data.username.clone().into()]),
        div.class("row")((
            div.class("col-sm mx-1 px-4")((
                h4("Information"),
                ul(li(p((
                    "Trust score:",
                    nbsp,
                    user_trust_badge(data.user_trust_count),
                )))),
            )),
            div.class("col-sm mx-1 px-4")((
                h4("Control"),
                ul((
                    li(if data.trusted {
                        post_button(
                            format!("/user/{}/untrust", data.username),
                            "btn-outline-warning",
                            "star",
                            "Cancel trust",
                        )
                    } else {
                        post_button(
                            format!("/user/{}/trust", data.username),
                            "btn-outline-primary",
                            "star",
                            "Trust",
                        )
                    }),
                    li(if data.flagged {
                        post_button(
                            format!("/user/{}/unflag", data.username),
                            "btn-outline-danger",
                            "flag",
                            "Cancel flag",
                        )
                    } else {
                        post_button(
                            format!("/user/{}/flag", data.username),
                            "btn-outline-danger",
                            "flag",
                            "Flag",
                        )
                    }),
                )),
            )),
        )),
        div.class("row justify-content-center")(h3("Reviews")),
        review::list(&data.reviews, review::ListMode::User),
    );
    base::base(&data.base, Box::new(content))
}
