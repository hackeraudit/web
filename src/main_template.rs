#![feature(universal_impl_trait)]
#![feature(try_from)]
#![feature(conservative_impl_trait)]
#![allow(unused)]
#![cfg_attr(feature = "cargo-clippy", allow(needless_pass_by_value))]

extern crate hackeraudit_api_common;
extern crate num;
#[macro_use]
extern crate num_derive;
extern crate semver;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate stpl;

extern crate failure;
#[macro_use]
extern crate failure_derive;

mod tpl;
mod data;
mod error;

fn main() {
    stpl::handle_dynamic()
        .template(&tpl::home_tpl())
        .template(&tpl::user_tpl())
        .template(&tpl::top_wanted_tpl())
        .template(&tpl::top_users_tpl())
        .template(&tpl::security_tpl())
        .template(&tpl::pkg_tpl())
        .template(&tpl::pkgver_tpl())
        .template(&tpl::register_tpl())
        .template(&tpl::search_tpl())
        .template(&tpl::unauthorized_tpl());
}
