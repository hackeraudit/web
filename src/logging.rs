use {slog, slog_async, slog_term};
use std;

use std::env;

use slog::Drain;

use std::net::UdpSocket;
//use cadence::{BufferedUdpMetricSink, QueuingMetricSink, StatsdClient, DEFAULT_PORT};
use cadence::{BufferedUdpMetricSink, QueuingMetricSink, StatsdClient};

use Result;

#[allow(unused)]
fn create_statsd() -> Result<StatsdClient> {
    let socket = UdpSocket::bind("0.0.0.0:0").unwrap();
    socket.set_nonblocking(true).unwrap();

    let socket_addr = env::var("STATSD_URL")?;

    let udp_sink = BufferedUdpMetricSink::from(socket_addr, socket)?;
    let queuing_sink = QueuingMetricSink::from(udp_sink);
    Ok(StatsdClient::from_sink("ha-web", queuing_sink))
}

pub fn create_logger(verbosity: u32) -> Result<slog::Logger> {
    let log = match verbosity {
        0 => slog::Logger::root(slog::Discard, o!()),
        v => {
            let _ = match v {
                0 => slog::Level::Warning,
                1 => slog::Level::Info,
                2 => slog::Level::Debug,
                _ => slog::Level::Trace,
            };
            let drain = slog_term::term_full();
            if verbosity > 4 {
                // at level 4, use synchronous logger so not to loose any
                // logging messages
                let drain = std::sync::Mutex::new(drain);
                let log = slog::Logger::root(drain.fuse(), o!());
                info!(
                    log,
                    "Using synchronized logging, that we'll be slightly slower."
                );
                log
            } else {
                let drain = slog_async::Async::default(drain.fuse());
                slog::Logger::root(drain.fuse(), o!())
            }
        }
    };
    Ok(log)
}
