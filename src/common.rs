use db;
use models;
use rocket::Outcome;
use rocket::http::Status;
use rocket::request::{self, FlashMessage, FromRequest, Request};
use serde_json;
use std;
use std::collections::BTreeMap;

use Result;
use hackeraudit_api_common::{UsageStats, UsageStatsItem};
use models::ReviewScore;

use data::*;

impl Session {
    pub fn to_json(&self) -> serde_json::Result<String> {
        serde_json::to_string(&self)
    }

    pub fn from_user(user: &models::User) -> Session {
        Session {
            uid: user.id,
            username: user.username.clone(),
            token: user.token.clone(),
        }
    }
}

impl<'a, 'r> FromRequest<'a, 'r> for Session {
    type Error = String;
    fn from_request(request: &'a Request<'r>) -> request::Outcome<Self, Self::Error> {
        if let Some(cookie) = request.cookies().get_private("session") {
            match serde_json::from_str(cookie.value()) {
                Err(_) => Outcome::Failure((
                    Status::BadRequest,
                    format!("Couldn't parse session: {}", cookie.value()),
                )),
                Ok(s) => Outcome::Success(s),
            }
        } else {
            Outcome::Forward(())
        }
    }
}

#[derive(Serialize)]
pub struct TeraFlash<'a> {
    pub type_: &'a str,
    pub msg: &'a str,
}

impl<'a> std::convert::From<&'a FlashMessage> for TeraFlash<'a> {
    fn from(flash: &'a FlashMessage) -> Self {
        TeraFlash {
            type_: match flash.name() {
                "success" => "success",
                "warning" => "warning",
                _ => "danger",
            },
            msg: flash.msg(),
        }
    }
}

pub fn map_to_usage_stat_item(map: BTreeMap<models::ReviewScore, i64>) -> UsageStatsItem {
    UsageStatsItem {
        used: *map.get(&ReviewScore::Used).unwrap_or(&0),
        bad: *map.get(&ReviewScore::Bad).unwrap_or(&0),
        good: *map.get(&ReviewScore::Good).unwrap_or(&0),
    }
}

pub fn calculate_package_stats(
    pkg_ver: &models::PackageVersion,
    db: &db::DbConn,
) -> Result<UsageStats> {
    // calculate stats
    let patch_map = models::PackageVersion::calculate_stats_patch(pkg_ver, db)?;
    let minor_map = models::PackageVersion::calculate_stats_minor(pkg_ver, db)?;
    let major_map = models::PackageVersion::calculate_stats_major(pkg_ver, db)?;
    let total_map = models::PackageVersion::calculate_stats_total(pkg_ver, db)?;

    Ok(UsageStats {
        patch: map_to_usage_stat_item(patch_map),
        minor: map_to_usage_stat_item(minor_map),
        major: map_to_usage_stat_item(major_map),
        total: map_to_usage_stat_item(total_map),
    })
}
