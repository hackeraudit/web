use error::*;
use hackeraudit_api_common as api_common;
use num::FromPrimitive;
use semver;
use std::convert::TryFrom;
use std::result;

#[derive(Serialize, Deserialize, Clone)]
pub struct Session {
    pub uid: i32,
    pub username: String,
    pub token: String,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Flash {
    pub msg: String,
    pub type_: String,
}

#[derive(FromPrimitive, ToPrimitive, Serialize, Deserialize, Copy, Clone, Ord, PartialOrd, Eq,
         PartialEq)]
pub enum ReviewScore {
    None = 0,
    Used = 1,
    Bad = 2,
    Good = 3,
}

impl ReviewScore {
    #[allow(dead_code)]
    pub fn all() -> &'static [ReviewScore] {
        &[
            ReviewScore::None,
            ReviewScore::Used,
            ReviewScore::Bad,
            ReviewScore::Good,
        ]
    }

    #[allow(unused)]
    fn from_i16(i: i16) -> Option<Self> {
        FromPrimitive::from_i16(i)
    }

    pub fn from_i16_default(i: i16) -> Self {
        FromPrimitive::from_i16(i).unwrap_or(ReviewScore::None)
    }

    pub fn from_str(s: &str) -> Option<Self> {
        match s {
            "none" => Some(ReviewScore::None),
            "used" => Some(ReviewScore::Used),
            "bad" => Some(ReviewScore::Bad),
            "good" => Some(ReviewScore::Good),
            _ => None,
        }
    }

    pub fn from_str_default(s: &str) -> Self {
        Self::from_str(s).unwrap_or(ReviewScore::None)
    }

    pub fn to_str(&self) -> &str {
        match *self {
            ReviewScore::None => "none",
            ReviewScore::Used => "used",
            ReviewScore::Bad => "bad",
            ReviewScore::Good => "good",
        }
    }

    #[allow(unused)]
    pub fn to_string(&self) -> String {
        self.to_str().into()
    }
}

#[derive(FromPrimitive, ToPrimitive, Copy, Clone, Serialize, Deserialize)]
pub enum SourceId {
    CratesIo = 0,
}

impl SourceId {
    pub fn from_i16(i: i16) -> Option<Self> {
        FromPrimitive::from_i16(i)
    }

    pub fn from_i16_default(i: i16) -> Self {
        FromPrimitive::from_i16(i).unwrap_or(SourceId::CratesIo)
    }

    pub fn from_str(s: &str) -> Option<Self> {
        match s {
            "crates.io" => Some(SourceId::CratesIo),
            _ => None,
        }
    }

    pub fn from_str_default(s: &str) -> Self {
        Self::from_str(s).unwrap_or(SourceId::CratesIo)
    }

    pub fn to_str(&self) -> &str {
        match *self {
            SourceId::CratesIo => "crates.io",
        }
    }

    pub fn to_string(&self) -> String {
        self.to_str().into()
    }

    pub fn to_lang_str(&self) -> &str {
        match *self {
            SourceId::CratesIo => "Rust",
        }
    }

    pub fn to_lang_string(&self) -> String {
        self.to_lang_str().into()
    }
}

#[derive(Copy, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct SimpleSemver {
    pub major: i16,
    pub minor: i16,
    pub patch: i16,
    pub pre: bool,
}

impl SimpleSemver {
    // TODO: Optimize
    pub fn to_string(&self) -> String {
        let sem: semver::Version = self.into();
        sem.to_string()
    }
}

impl TryFrom<semver::Version> for SimpleSemver {
    type Error = InvalidPkgVersion;
    fn try_from(sv: semver::Version) -> result::Result<Self, Self::Error> {
        Ok(SimpleSemver {
            major: FromPrimitive::from_u64(sv.major).ok_or(InvalidPkgVersion)?,
            minor: FromPrimitive::from_u64(sv.minor).ok_or(InvalidPkgVersion)?,
            patch: FromPrimitive::from_u64(sv.patch).ok_or(InvalidPkgVersion)?,
            pre: sv.is_prerelease(),
        })
    }
}

impl<'a> TryFrom<&'a semver::Version> for SimpleSemver {
    type Error = InvalidPkgVersion;
    fn try_from(sv: &semver::Version) -> result::Result<Self, Self::Error> {
        TryFrom::try_from(sv.clone())
    }
}

impl From<SimpleSemver> for semver::Version {
    fn from(ssv: SimpleSemver) -> Self {
        semver::Version {
            major: ssv.major as u64,
            minor: ssv.minor as u64,
            patch: ssv.patch as u64,
            pre: if ssv.pre {
                vec![semver::Identifier::AlphaNumeric("pre".into())]
            } else {
                vec![]
            },
            build: vec![],
        }
    }
}
impl<'a> From<&'a SimpleSemver> for semver::Version {
    fn from(ssv: &SimpleSemver) -> Self {
        From::from(*ssv)
    }
}

pub type Count = i64;
pub type Counts = api_common::UsageStatsItem;
