use RequestResult;
use ctx;
use data::SourceId;
use models;
use rocket::request::FlashMessage;
use tpl;

#[get("/top/wanted/<source>")]
fn top_wanted(
    ctx: ctx::Ctx,
    source: String,
    flash: Option<FlashMessage>,
) -> RequestResult<tpl::Rendered> {
    let source_id = SourceId::from_str_default(&source);

    let wanted = models::PackageVersion::load_most_wanted(source_id, &ctx.db)?;

    let data = tpl::top::wanted::Data {
        base: ctx.base_data(flash),
        source_id: source_id,
        all_time: wanted
            .into_iter()
            .map(|(pkg, pkgver, counts)| tpl::top::wanted::Item {
                name: pkg.name,
                version: pkgver.to_simplesemver(),
                review_ratio: counts.review_ratio_cents,
            })
            .collect(),
        fresh: vec![],
    };

    Ok(tpl::render(&tpl::top_wanted_tpl(), &data))
}

#[get("/top/users")]
fn top_users(ctx: ctx::Ctx, flash: Option<FlashMessage>) -> RequestResult<tpl::Rendered> {
    let trusted = models::User::load_most_trusted(&ctx.db)?;

    let data = tpl::top::users::Data {
        base: ctx.base_data(flash),
        users: trusted
            .into_iter()
            .map(|u| tpl::top::users::Item {
                username: u.username,
                trust_count: u.trust_dst_count as u32,
            })
            .collect(),
    };

    Ok(tpl::render(&tpl::top_users_tpl(), &data))
}
