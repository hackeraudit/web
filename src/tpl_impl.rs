use rocket::http::ContentType;
use rocket::request::Request;
use rocket::response::{self, Responder, Response};
use std::io::Cursor;
use tpl;

impl<'r> Responder<'r> for tpl::Rendered {
    fn respond_to(self, _: &Request) -> response::Result<'r> {
        Response::build()
            .sized_body(Cursor::new(self.0))
            .header(ContentType::HTML)
            .ok()
    }
}
