use super::*;

use std::fmt;

#[derive(Debug)]
pub struct RequestError {
    pub cause: failure::Error,
}

#[derive(Debug, Fail)]
#[fail(display = "Entity not found")]
pub struct NotFound;

#[derive(Debug, Fail)]
#[fail(display = "Invalid package version")]
pub struct InvalidPkgVersion;

#[derive(Debug, Fail)]
#[fail(display = "Invalid package name")]
pub struct InvalidPkgName;

/* kurwa
impl<T> std::convert::From<T> for RequestError
where
    T: failure::Fail,
{
    fn from(f: T) -> RequestError {
        RequestError { cause: f.into() }
    }
}
*/

impl std::convert::From<failure::Error> for RequestError {
    fn from(f: failure::Error) -> RequestError {
        RequestError { cause: f }
    }
}

pub trait ToError {
    type Output;
    fn into_error(self) -> Self::Output;
}

impl<O, E: failure::Fail> ToError for std::result::Result<O, E> {
    type Output = std::result::Result<O, failure::Error>;
    fn into_error(self) -> Self::Output {
        match self {
            Ok(o) => Ok(o),
            Err(e) => Err(e.into()),
        }
    }
}

impl fmt::Display for RequestError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Request Error: {}", self.cause.cause())
    }
}
