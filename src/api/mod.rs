use Error;
use RequestResult;
use Result;
use ctx;
use data::ReviewScore;
use data::SourceId;
use db::DbConn;
use diesel::prelude::*;
use hackeraudit_api_common::{AuditRequest, AuditResponse, AuditResponsePackage, UsageStats};
use insideout::InsideOut;
use models;
use num::ToPrimitive;
use rate_limit::HostOrUser;
use rocket_contrib::Json;
use schema::*;
use semver;
use std::convert::TryInto;
use worker::WorkerPool;

#[derive(Debug, Fail)]
#[fail(display = "Invalid token")]
pub struct InvalidToken;

#[derive(Debug, Fail)]
#[fail(display = "Invalid source id")]
pub struct InvalidSourceId;

#[post("/api/0/audit", data = "<request>")]
fn audit(
    request: Json<AuditRequest>,
    db: DbConn,
    worker: WorkerPool,
    ctx: ctx::Ctx,
) -> RequestResult<Json<AuditResponse>> {
    ctx.rate_limit()?;

    let mut resp = vec![];
    let user = if let Some(token) = request.token.as_ref() {
        Some(models::User::load_by_token(token, &db)?.ok_or_else(|| Error::from(InvalidToken))?)
    } else {
        None
    };

    ctx.rate_limiter.audit(
        user.as_ref()
            .map(|u| HostOrUser::U(u.id))
            .unwrap_or_else(|| HostOrUser::H(ctx.info.remote.ip())),
    )?;

    for req_pkg in &request.packages {
        println!("{:?}", req_pkg);
        let res = (|| -> Result<AuditResponsePackage> {
            let ssv: models::SimpleSemver = semver::Version::parse(&req_pkg.version)?.try_into()?;

            if let Some((version, package)) = package_versions::table
                .inner_join(packages::table)
                .filter(packages::name.eq(&req_pkg.name))
                .filter(package_versions::major.eq(ssv.major))
                .filter(package_versions::minor.eq(&ssv.minor))
                .filter(package_versions::patch.eq(&ssv.patch))
                .filter(package_versions::pre.eq(&ssv.pre))
                .first::<(models::PackageVersion, models::Package)>(&*db)
                .optional()?
            {
                if let Some(user) = user.as_ref() {
                    let res = models::PackageVersionReview::create_if_does_not_exist(
                        user.id,
                        version.id,
                        models::ReviewScore::Used.to_i16().unwrap(),
                        "",
                        &*db,
                    )?;

                    if res.is_some() {
                        println!("Recalculating stuff");
                        version.recalculate_package_stats(&*db)?;
                    } else {
                        println!("NOT Recalculating stuff");
                    }
                }

                let review: Option<models::PackageVersionReview> = user.as_ref()
                    .map(|user| {
                        package_version_reviews::table
                            .filter(package_version_reviews::user_id.eq(&user.id))
                            .filter(package_version_reviews::package_version_id.eq(&version.id))
                            .first::<models::PackageVersionReview>(&*db)
                            .optional()
                    })
                    .inside_out()?
                    .unwrap_or(None);

                let stats = version.load_usage_stats(&*db)?;

                Ok(AuditResponsePackage {
                    stats: stats,
                    self_score_bad: review
                        .as_ref()
                        .map(|r| ReviewScore::from_i16_default(r.score) == ReviewScore::Bad)
                        .unwrap_or(false),
                    self_score_good: review
                        .as_ref()
                        .map(|r| ReviewScore::from_i16_default(r.score) == ReviewScore::Good)
                        .unwrap_or(false),
                    url: Some(format!(
                        "{}/package/{}/{}/{}",
                        ::tpl::misc::url_base(),
                        SourceId::from_i16_default(package.source).to_string(),
                        package.name,
                        version.to_semver()
                    )),
                })
            } else {
                let source_id = SourceId::from_str(&req_pkg.source).ok_or(InvalidSourceId)?;

                let user_id = user.as_ref().map(|user| user.id);
                worker.evaluate_new_package(
                    source_id,
                    req_pkg.name.clone(),
                    req_pkg.version.clone(),
                    user_id,
                );

                Ok(AuditResponsePackage {
                    stats: UsageStats::empty(),
                    self_score_bad: false,
                    self_score_good: false,
                    url: None,
                })
            }
        })();
        resp.push(res.map_err(|e| e.to_string()))
    }

    Ok(Json(AuditResponse {
        packages: resp,
        username: user.map(|u| u.username),
    }))
}
