use RequestResult;
use Session;
use ctx;
use error::NotFound;
use failure::Error;
use insideout::InsideOut;
use models;
use rocket::request::FlashMessage;
use rocket::response::{Flash, Redirect};
use std::convert::TryInto;
use tpl;

#[get("/user/<username>")]
fn user(
    ctx: ctx::Ctx,
    username: String,
    _session: Option<Session>,
    flash: Option<FlashMessage>,
) -> RequestResult<tpl::Rendered> {
    ctx.rate_limit()?;

    let user =
        models::User::load_by_username(&username, &ctx.db)?.ok_or_else(|| Error::from(NotFound))?;
    let review_list = models::PackageVersionReview::load_by_username(&username, &ctx.db)?;

    let trusted = ctx.get_session()
        .ok()
        .map(|s| models::UserTrust::exists(s.uid, user.id, &ctx.db))
        .inside_out()?
        .unwrap_or(false);

    let flagged = ctx.get_session()
        .ok()
        .map(|s| models::UserFlag::exists(s.uid, user.id, &ctx.db))
        .inside_out()?
        .unwrap_or(false);

    let data = tpl::user::Data {
        base: ctx.base_data(flash),
        user_trust_count: user.trust_dst_count as u32,
        flagged: flagged,
        trusted: trusted,
        reviews: review_list
            .into_iter()
            .map(|mr| tpl::review::Data {
                username: username.clone(),
                user_trust_count: user.trust_dst_count as u32,

                pkg_version: mr.version.try_into().unwrap(),
                pkg_name: mr.name,
                comment: mr.comment,
                score: mr.score,
                source_id: mr.source_id,
            })
            .collect(),
        username: username,
    };

    Ok(tpl::render(&tpl::user_tpl(), &data))
}

#[post("/user/<username>/trust")]
fn user_trust(ctx: ctx::Ctx, username: String) -> RequestResult<Flash<Redirect>> {
    ctx.rate_limit()?;
    let session = ctx.get_session()?;
    // TODO: User a separate rate limiter?
    let dst_user =
        models::User::load_by_username(&username, &ctx.db)?.ok_or_else(|| Error::from(NotFound))?;
    models::UserTrust::set_trust(session.uid, dst_user.id, &ctx.db)?;

    Ok(Flash::success(
        Redirect::to(&format!("/user/{}", username)),
        "User's trust score increased".to_owned(),
    ))
}

#[get("/user/<username>/trust")]
fn user_trust_get(username: String) -> Redirect {
    Redirect::to(&format!("/user/{}", username))
}

#[post("/user/<username>/untrust")]
fn user_untrust(ctx: ctx::Ctx, username: String) -> RequestResult<Flash<Redirect>> {
    ctx.rate_limit()?;
    let session = ctx.get_session()?;
    // TODO: User a separate rate limiter?
    let dst_user =
        models::User::load_by_username(&username, &ctx.db)?.ok_or_else(|| Error::from(NotFound))?;
    models::UserTrust::unset_trust(session.uid, dst_user.id, &ctx.db)?;

    Ok(Flash::success(
        Redirect::to(&format!("/user/{}", username)),
        "User's trust score decreased".to_owned(),
    ))
}

#[get("/user/<username>/untrust")]
fn user_untrust_get(username: String) -> Redirect {
    Redirect::to(&format!("/user/{}", username))
}

#[get("/token")]
fn self_token() -> Redirect {
    Redirect::to("/self/security#token")
}

#[get("/self/security")]
fn self_security(ctx: ctx::Ctx, flash: Option<FlashMessage>) -> RequestResult<tpl::Rendered> {
    ctx.rate_limit()?;
    let session = ctx.get_session()?;

    let data = tpl::security::Data {
        base: ctx.base_data(flash),
        token: session.token.clone(),
    };

    Ok(tpl::render(&tpl::security_tpl(), &data))
}

#[post("/user/<username>/flag")]
fn user_flag(ctx: ctx::Ctx, username: String) -> RequestResult<Flash<Redirect>> {
    ctx.rate_limit()?;
    let session = ctx.get_session()?;
    // TODO: User a separate rate limiter?
    let dst_user =
        models::User::load_by_username(&username, &ctx.db)?.ok_or_else(|| Error::from(NotFound))?;
    models::UserFlag::set_flag(session.uid, dst_user.id, &ctx.db)?;

    Ok(Flash::success(
        Redirect::to(&format!("/user/{}", username)),
        "User's flag score increased".to_owned(),
    ))
}

#[get("/user/<username>/flag")]
fn user_flag_get(username: String) -> Redirect {
    Redirect::to(&format!("/user/{}", username))
}

#[post("/user/<username>/unflag")]
fn user_unflag(ctx: ctx::Ctx, username: String) -> RequestResult<Flash<Redirect>> {
    ctx.rate_limit()?;
    let session = ctx.get_session()?;
    // TODO: User a separate rate limiter?
    let dst_user =
        models::User::load_by_username(&username, &ctx.db)?.ok_or(Error::from(NotFound))?;
    models::UserFlag::unset_flag(session.uid, dst_user.id, &ctx.db)?;

    Ok(Flash::success(
        Redirect::to(&format!("/user/{}", username)),
        "User's flag score decreased".to_owned(),
    ))
}

#[get("/user/<username>/unflag")]
fn user_unflag_get(username: String) -> Redirect {
    Redirect::to(&format!("/user/{}", username))
}
