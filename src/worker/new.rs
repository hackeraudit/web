use super::AddPackageData;
use Result;
use chrono;
use data::SourceId;
use db;
use error::{InvalidPkgName, InvalidPkgVersion};
use failure::ResultExt;
use models;
use num::ToPrimitive;
use reqwest;
use reqwest::header::qitem;
use reqwest::mime;
use semver;

fn validate_name(s: &str) -> Result<()> {
    if s.is_empty() {
        Err(InvalidPkgName)?;
    }

    let res = s.chars()
        .all(|ch| ch.is_alphanumeric() || ".-_".chars().any(|vch| ch == vch));

    if !res {
        Err(InvalidPkgName)?;
    }

    Ok(())
}

#[test]
fn validate_name_test() {
    assert!(validate_name("asdfasdf-sdf").is_ok());
    assert!(validate_name("asdfasdf/sdf").is_err());
}

fn validate_version(s: &str) -> Result<()> {
    semver::Version::parse(s).context(InvalidPkgVersion)?;
    Ok(())
}

#[test]
fn validate_version_test() {
    assert!(validate_version("3.2.3-foobar").is_ok());
    assert!(validate_version("3.2.3-foobar/bar").is_err());
    assert!(validate_version("asdfasdf/sdf").is_err());
}

#[derive(Deserialize, Serialize, Debug)]
struct CrateInfo {
    version: CrateVersionInfo,
}

#[derive(Deserialize, Serialize, Debug)]
struct CrateVersionInfo {
    updated_at: chrono::DateTime<chrono::offset::Utc>,
    created_at: chrono::DateTime<chrono::offset::Utc>,
    yanked: bool,
}

pub(crate) fn check_and_add_package(
    data: AddPackageData,
    client: &reqwest::Client,
    db: &db::Pool,
) -> Result<()> {
    validate_name(&data.name)?;
    validate_version(&data.version)?;

    match data.source {
        SourceId::CratesIo => {
            let url = format!(
                "https://crates.io/api/v1/crates/{}/{}",
                data.name, data.version
            );

            let mut resp = client
                .get(&url)
                .header(reqwest::header::Accept(vec![qitem(mime::APPLICATION_JSON)]))
                .send()?;

            println!("Resp received: {:?}", resp);

            let ver_info: CrateInfo = resp.json()?;

            println!("Version info: {:?}", ver_info);

            let sv = semver::Version::parse(&data.version)?;
            let pkgver = models::PackageVersion::create_if_not_exists(
                SourceId::CratesIo,
                &data.name,
                &sv,
                &*db.get()?,
            )?;

            if let Some(user_id) = data.user_id {
                let res = models::PackageVersionReview::create_if_does_not_exist(
                    user_id,
                    pkgver.id,
                    models::ReviewScore::Used.to_i16().unwrap(),
                    "",
                    &*db.get()?,
                )?;

                if res.is_some() {
                    pkgver.recalculate_package_stats(&*db.get()?)?;
                }
            }
        }
    }
    Ok(())
}
