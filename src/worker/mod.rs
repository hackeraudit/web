use Result;
use data::SourceId;
use db;
use failure::ResultExt;
use models;
use reqwest;
use rocket::{Outcome, Request, State};
use rocket::request::{self, FromRequest};
use std::{sync, thread, time};

mod new;

pub(crate) struct AddPackageData {
    source: SourceId,
    name: String,
    version: String,
    user_id: Option<models::UserId>,
}

enum Message {
    AddPackage(AddPackageData),
}

struct WorkerPoolSyncInner {
    tx: sync::mpsc::Sender<Message>,
    _join: thread::JoinHandle<Result<()>>,
}

pub struct WorkerPoolSync {
    inner: sync::Mutex<WorkerPoolSyncInner>,
}

/// Connection request guard type: a wrapper around an r2d2 pooled connection.
pub struct WorkerPool(sync::mpsc::Sender<Message>);

impl WorkerPool {
    pub fn evaluate_new_package(
        &self,
        source: SourceId,
        name: String,
        version: String,
        user_id: Option<models::UserId>,
    ) {
        self.0
            .send(Message::AddPackage(AddPackageData {
                source: source,
                name: name,
                version: version,
                user_id: user_id,
            }))
            .expect("send failed");
    }
}

impl<'a, 'r> FromRequest<'a, 'r> for WorkerPool {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<WorkerPool, ()> {
        let state = request.guard::<State<WorkerPoolSync>>()?;
        let inner = state.inner.lock().unwrap();
        Outcome::Success(WorkerPool(inner.tx.clone()))
    }
}

pub(crate) fn init(db: db::Pool) -> WorkerPoolSync {
    let (tx, rx) = sync::mpsc::channel();

    let join = thread::spawn(move || {
        let c = reqwest::Client::builder()
            .timeout(time::Duration::from_secs(3))
            .build()
            .context("Can't build http client")?;
        let db = db;
        loop {
            let m = rx.recv()?;
            let res = match m {
                Message::AddPackage(data) => new::check_and_add_package(data, &c, &db),
            };

            println!("{:?}", res);
        }
    });

    WorkerPoolSync {
        inner: sync::Mutex::new(WorkerPoolSyncInner {
            tx: tx,
            _join: join,
        }),
    }
}
