#![feature(custom_attribute)]
#![feature(attr_literals)]
#![feature(try_from)]
#![feature(nll)]
#![feature(plugin)]
#![plugin(rocket_codegen)]
#![feature(custom_derive)]
#![feature(universal_impl_trait)]
#![feature(conservative_impl_trait)]
#![recursion_limit = "1000"]
#![allow(unused_attributes)]
#![cfg_attr(feature = "cargo-clippy", allow(needless_pass_by_value))]

extern crate cadence;
extern crate chrono;
#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_infer_schema;
extern crate dotenv;
extern crate failure;
#[macro_use]
extern crate failure_derive;
extern crate flate2;
extern crate futures;
extern crate hackeraudit_api_common;
extern crate hubcaps;
extern crate hyper;
extern crate hyper_native_tls;
extern crate insideout;
extern crate lru_time_cache;
extern crate num;
#[macro_use]
extern crate num_derive;
extern crate r2d2;
extern crate r2d2_diesel;
extern crate rand;
extern crate reqwest;
extern crate rocket;
#[macro_use]
extern crate rocket_contrib;
extern crate semver;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;
#[macro_use]
extern crate slog;
extern crate slog_async;
extern crate slog_term;
extern crate stpl;
extern crate tokio_core;

use dotenv::dotenv;

mod error;
mod error_rocket;
mod rate_limit;
mod package;
mod ctx;
// temporarily not used
//mod gzip;
mod search;
mod models;
mod schema;
mod user;
mod data;

use data::*;

use std::path::{Path, PathBuf};

use rocket::Outcome;
use rocket::request::FlashMessage;
use rocket::response::NamedFile;

mod tpl;
mod tpl_impl;

mod db;
mod logging;
mod auth;

mod cookie;

mod api;

mod top;
mod worker;

mod common;

use failure::Error;

type Result<T> = std::result::Result<T, failure::Error>;
type RequestResult<T> = std::result::Result<T, error::RequestError>;

#[get("/")]
fn home(ctx: ctx::Ctx, flash: Option<FlashMessage>) -> tpl::Rendered {
    let data = tpl::home::Data {
        base: ctx.base_data(flash),
    };

    tpl::render(&tpl::home_tpl(), &data)
}

impl From<FlashMessage> for data::Flash {
    fn from(f: FlashMessage) -> Self {
        ::data::Flash {
            msg: f.msg().into(),
            type_: match f.name() {
                "success" => "success",
                "warning" => "warning",
                _ => "danger",
            }.into(),
        }
    }
}

#[get("/static/<file..>", rank = 0)]
fn static_static_files(file: PathBuf) -> Option<NamedFile> {
    NamedFile::open(Path::new("static/").join(file)).ok()
}

#[get("/<file..>", rank = 1)]
fn static_files(file: PathBuf) -> Option<NamedFile> {
    eprintln!("Warning, wrong path {}", file.display());
    None
}

fn main() {
    dotenv().ok();

    let db_pool = db::init();

    rocket::ignite()
        .manage(logging::create_logger(1).unwrap())
        .manage(db_pool.clone())
        .manage(worker::init(db_pool))
        .manage(rate_limit::RateLimiter::new())
        .mount(
            "/",
            routes![
                home,
                search::search,
                search::search_q,
                package::package,
                package::package_version,
                package::package_version_review,
                top::top_wanted,
                top::top_users,
                user::user,
                user::user_trust,
                user::user_trust_get,
                user::user_untrust,
                user::user_untrust_get,
                user::user_flag,
                user::user_flag_get,
                user::user_unflag,
                user::user_unflag_get,
                user::self_token,
                user::self_security,
                static_files,
                static_static_files,
                auth::github_callback,
                auth::github_login,
                auth::github_login_src,
                auth::logout,
                auth::register_get,
                auth::register_post,
                api::audit,
            ],
        )
        .launch();
}
