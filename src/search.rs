use rocket::request::FlashMessage;

use ctx;
use tpl;

use Result;
use data::SourceId;
use models;

#[derive(FromForm)]
struct SearchForm {
    q: String,
    p: Option<u64>,
}

#[get("/package/search")]
fn search(ctx: ctx::Ctx, flash: Option<FlashMessage>) -> Result<tpl::Rendered> {
    search_q(None, ctx, flash)
}

#[get("/package/search?<query>")]
fn search_q(
    query: Option<SearchForm>,
    ctx: ctx::Ctx,
    flash: Option<FlashMessage>,
) -> Result<tpl::Rendered> {
    let query = query.unwrap_or_else(|| SearchForm {
        q: "".into(),
        p: None,
    });

    const PER_PAGE: u64 = 20;
    let mut res = models::Package::query_names(
        &query.q,
        PER_PAGE as i64 * query.p.unwrap_or(0) as i64,
        20 * PER_PAGE as i64,
        &ctx.db,
    )?;
    let page = query.p.unwrap_or(0) as u64;
    let data = tpl::search::Data {
        base: ctx.base_data(flash),
        query: query.q,
        total_pages: ((res.len() as u64 + PER_PAGE - 1) / PER_PAGE as u64) as u64
            + query.p.unwrap_or(0) as u64,
        total: (page * PER_PAGE) as u64 + res.len() as u64,
        max: (page * PER_PAGE) as u64 + (PER_PAGE * 20) as u64,
        results: res.drain(..)
            .take(PER_PAGE as usize)
            .map(|(s, n)| {
                let id = SourceId::from_i16(s).unwrap();
                tpl::search::SearchResult {
                    name: n,
                    source_id: id,
                }
            })
            .collect(),
        page: page,
    };

    Ok(tpl::render(&tpl::search_tpl(), &data))
}
