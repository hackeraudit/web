use super::*;

use rocket::http::{ContentType, Status};
use rocket::response::{Responder, Response};
use std::io::Cursor;

use rocket::request::Request;

use error::*;

// TODO: This stuff should log instead of responding with error
impl<'r> Responder<'r> for RequestError {
    fn respond_to(self, request: &Request) -> ::std::result::Result<Response<'r>, Status> {
        // Create JSON response
        let resp = json!({
                "status": "failure",
                "message": format!("{}", self),
            }).to_string();

        let cause = self.cause.cause();

        println!("{:?}", cause);
        let status = if let Some(_e) = cause.downcast_ref::<rate_limit::RateLimitErr>() {
            Status::TooManyRequests
        } else if let Some(_e) = cause.downcast_ref::<api::InvalidToken>() {
            Status::Unauthorized
        } else if let Some(e) = cause.downcast_ref::<ctx::Unauthorized>() {
            let data = tpl::unauthorized::Data {
                base: e.ctx.lock().unwrap().base_data(None),
            };

            let mut resp = ::tpl::render(&tpl::unauthorized_tpl(), &data).respond_to(request)?;
            resp.set_status(Status::Unauthorized);

            return Ok(resp);
        } else {
            Status::BadRequest
        };

        Ok(Response::build()
            .status(status)
            .header(ContentType::JSON)
            .sized_body(Cursor::new(resp))
            .finalize())
    }
}
