CREATE TABLE users (
  id SERIAL PRIMARY KEY,
  username VARCHAR NOT NULL,
  about VARCHAR NOT NULL DEFAULT '',
  name VARCHAR NOT NULL DEFAULT '',
  token VARCHAR NOT NULL DEFAULT md5(random()::text),
  trust_dst_count INTEGER NOT NULL DEFAULT 0,
  flag_dst_count INTEGER NOT NULL DEFAULT 0
);

CREATE UNIQUE INDEX user_by_username ON users (username);
CREATE UNIQUE INDEX user_by_token ON users (token);
CREATE INDEX users_by_trust_count ON users (trust_dst_count);
CREATE INDEX users_by_flag_count ON users (flag_dst_count);

CREATE TABLE auths (
  id SERIAL PRIMARY KEY,
  user_id INT not NULL REFERENCES users,
  service SMALLINT not NULL,
  service_uid VARCHAR not NULL
);

CREATE INDEX auth_by_user_id_idx ON auths (user_id);

CREATE INDEX auth_service_id_idx ON auths (service, service_uid);

CREATE TABLE user_trusts (
  src_user_id INT not NULL REFERENCES users,
  dst_user_id INT not NULL REFERENCES users,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  PRIMARY KEY (src_user_id, dst_user_id)
);

CREATE TABLE user_flags (
  src_user_id INT not NULL REFERENCES users,
  dst_user_id INT not NULL REFERENCES users,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  PRIMARY KEY (src_user_id, dst_user_id)
);

CREATE TABLE packages (
  id SERIAL PRIMARY KEY,
  source SMALLINT not NULL,
  name VARCHAR not NULL
);

CREATE UNIQUE INDEX package_by_name ON packages (name, source);

CREATE TABLE package_versions (
  id BIGSERIAL PRIMARY KEY,
  package_id INT not NULL REFERENCES packages,
  major SMALLINT not NULL,
  minor SMALLINT not NULL,
  patch SMALLINT not NULL,
  pre BOOLEAN not NULL DEFAULT FALSE,
  created_at TIMESTAMP NOT NULL DEFAULT now()
);

CREATE UNIQUE INDEX package_version_by_version ON package_versions (package_id, major, minor, patch, pre);
CREATE INDEX package_version_by_created ON package_versions (created_at);

CREATE TABLE pkgver_total_counts (
  package_id INT not NULL REFERENCES packages,
  good BIGINT not NULL DEFAULT 0,
  bad BIGINT not NULL DEFAULT 0,
  used BIGINT not NULL DEFAULT 0,
  PRIMARY KEY (package_id)
);

CREATE TABLE pkgver_major_counts (
  package_id INT not NULL REFERENCES packages,
  major SMALLINT not NULL,
  good BIGINT not NULL DEFAULT 0,
  bad BIGINT not NULL DEFAULT 0,
  used BIGINT not NULL DEFAULT 0,
  PRIMARY KEY (package_id, major)
);

CREATE TABLE pkgver_minor_counts (
  package_id INT not NULL REFERENCES packages,
  major SMALLINT not NULL,
  minor SMALLINT not NULL,
  good BIGINT not NULL DEFAULT 0,
  bad BIGINT not NULL DEFAULT 0,
  used BIGINT not NULL DEFAULT 0,
  PRIMARY KEY (package_id, major, minor)
);


CREATE TABLE pkgver_patch_counts (
  package_id INT not NULL REFERENCES packages,
  major SMALLINT not NULL,
  minor SMALLINT not NULL,
  patch SMALLINT not NULL,
  pre BOOLEAN not NULL,
  good BIGINT not NULL DEFAULT 0,
  bad BIGINT not NULL DEFAULT 0,
  used BIGINT not NULL DEFAULT 0,
  review_ratio_cents BIGINT not NULL DEFAULT 0,
  PRIMARY KEY (package_id, major, minor, patch, pre)
);

CREATE INDEX pkgver_patch_counts_by_review_ratio ON pkgver_patch_counts (review_ratio_cents);

CREATE TABLE package_version_reviews (
  user_id INT not NULL REFERENCES users,
  package_version_id BIGINT not NULL REFERENCES package_versions,
  score SMALLINT not NULL DEFAULT 0,
  comment VARCHAR not NULL DEFAULT '',
  PRIMARY KEY (user_id, package_version_id),
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  updated_at TIMESTAMP NOT NULL DEFAULT now()
);
