DROP TABLE pkgver_patch_counts;
DROP TABLE pkgver_minor_counts;
DROP TABLE pkgver_major_counts;
DROP TABLE pkgver_total_counts;

DROP TABLE user_trusts;
DROP TABLE user_flags;

DROP TABLE package_version_reviews;

DROP TABLE package_versions;
DROP TABLE packages;

DROP TABLE auths;
DROP TABLE users;
