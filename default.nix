{ pkgs ? (import <nixpkgs> {})}:

let
  nightly = pkgs.rustChannelOf { date = "2017-12-15"; channel = "nightly"; };
  rustPlatform = pkgs.recurseIntoAttrs (pkgs.makeRustPlatform {
    rustc = nightly.rust;
    cargo = nightly.cargo;
  });
in
rustPlatform.buildRustPackage rec {
  name = "hackeraudit-web-${version}";
  version = "HEAD";

  src = builtins.filterSource (p: t:
      pkgs.lib.cleanSourceFilter p t &&
      baseNameOf p != "target" &&
      baseNameOf p != "rusty-tags.vi"
  )  ./. ;

   buildInputs = [
     pkgs.openssl
     pkgs.postgresql
  ];

  nativeBuildInputs = [ pkgs.pkgconfig ];

  cargoSha256 = "1cpdsfjyzvkpw97i3714yx2w7649k2pxm89h8gd18x4q7jml25sp";
}
