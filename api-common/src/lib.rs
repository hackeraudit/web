extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;

#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Serialize, Deserialize, Clone)]
pub struct AuditRequestPackage {
    pub source: String,
    pub name: String,
    pub version: String,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AuditRequest {
    pub packages: Vec<AuditRequestPackage>,
    pub token: Option<String>,
}

#[derive(Debug, Serialize, Deserialize, Default, Clone, Copy)]
pub struct UsageStatsItem {
    pub bad: i64,
    pub good: i64,
    pub used: i64,
}

impl UsageStatsItem {
    pub fn empty() -> Self {
        UsageStatsItem {
            bad: 0,
            good: 0,
            used: 0,
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Default, Clone)]
pub struct UsageStats {
    pub patch: UsageStatsItem,
    pub minor: UsageStatsItem,
    pub major: UsageStatsItem,
    pub total: UsageStatsItem,
}

impl UsageStats {
    pub fn empty() -> Self {
        UsageStats {
            patch: UsageStatsItem::empty(),
            minor: UsageStatsItem::empty(),
            major: UsageStatsItem::empty(),
            total: UsageStatsItem::empty(),
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AuditResponsePackage {
    pub stats: UsageStats,
    pub self_score_good: bool,
    pub self_score_bad: bool,
    pub url: Option<String>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AuditResponse {
    pub packages: Vec<Result<AuditResponsePackage, String>>,
    pub username: Option<String>,
}
